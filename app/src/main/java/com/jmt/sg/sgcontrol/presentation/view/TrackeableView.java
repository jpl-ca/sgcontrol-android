package com.jmt.sg.sgcontrol.presentation.view;

import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackeableView extends BaseView {
    void showData(AgentTrackeable agentTrackeable);
    void showErrorMessage(String message);
}