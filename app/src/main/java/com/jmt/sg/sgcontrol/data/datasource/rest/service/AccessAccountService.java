package com.jmt.sg.sgcontrol.data.datasource.rest.service;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.model.LoginTrackeableResponse;
import com.jmt.sg.sgcontrol.data.model.ValidateOrganizationResponse;
import com.jmt.sg.sgcontrol.data.datasource.rest.api.ApiService;

import org.json.JSONObject;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class AccessAccountService extends ApiService{
    private LoginApi apiService;

    public AccessAccountService(Context context) {
        super(context);
        apiService = retrofit.create(LoginApi.class);
    }

    public LoginApi getApi() {
        return apiService;
    }

    public interface LoginApi{
        @GET(_api+"/auth/me")
        Observable<LoginTrackeableResponse> getSessionMe();

        @FormUrlEncoded
        @POST(_api+"/auth/login")
        Observable<LoginTrackeableResponse> userLoginAgent(@Field("authentication_code") String authentication_code, @Field("password") String password);

        @FormUrlEncoded
        @POST(_api+"/organization/validate")
        Observable<ValidateOrganizationResponse> validateOrganization(@Field("name") String organization_name);

        @FormUrlEncoded
        @POST(_api+"/auth/me/update-notification-id")
        Observable<JSONObject> updateNotificationId(@Field("notification_id") String notification_id);

        @GET(_api+"/auth/logout")
        Observable<JSONObject> logout();
    }
}