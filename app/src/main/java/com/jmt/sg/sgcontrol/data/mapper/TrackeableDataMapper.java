package com.jmt.sg.sgcontrol.data.mapper;

import com.jmt.sg.sgcontrol.data.model.AgentTrackeableEntity;
import com.jmt.sg.sgcontrol.data.model.LoginTrackeableResponse;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/16/16.
 */
public class TrackeableDataMapper {

    public AgentTrackeable transformAgent(AgentTrackeableEntity agentTrackeableEntity) {
        AgentTrackeable agentTrackeable = new AgentTrackeable();
        if(agentTrackeableEntity==null)return agentTrackeable;

        agentTrackeable.setId(agentTrackeableEntity.getId());
        agentTrackeable.setAuthentication_code(agentTrackeableEntity.getAuthentication_code());
        agentTrackeable.setFirst_name(agentTrackeableEntity.getFirst_name());
        agentTrackeable.setLast_name(agentTrackeableEntity.getLast_name());
        agentTrackeable.setType(agentTrackeableEntity.getType());
        agentTrackeable.setToken(agentTrackeableEntity.getToken());
        agentTrackeable.setOrganization_id(agentTrackeableEntity.getOrganization_id());
        agentTrackeable.setLocation_frequency(agentTrackeableEntity.getLocation_frequency());
        return agentTrackeable;
    }

    public AgentTrackeable transformAgentResponse(LoginTrackeableResponse loginTrackeableResponse) {
        AgentTrackeable agentTrackeable = new AgentTrackeable();
        if(loginTrackeableResponse ==null)return agentTrackeable;

        agentTrackeable.setId(loginTrackeableResponse.getId());
        agentTrackeable.setAuthentication_code(loginTrackeableResponse.getAuthentication_code());
        agentTrackeable.setFirst_name(loginTrackeableResponse.getFirst_name());
        agentTrackeable.setLast_name(loginTrackeableResponse.getLast_name());
        agentTrackeable.setType(loginTrackeableResponse.getType());
        agentTrackeable.setToken(loginTrackeableResponse.getToken());
        agentTrackeable.setOrganization_id(loginTrackeableResponse.getOrganization_id());
        agentTrackeable.setLocation_frequency(loginTrackeableResponse.getLocation_frequency());
        return agentTrackeable;
    }

}