package com.jmt.sg.sgcontrol.data.datasource.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jmtech on 5/18/16.
 */
public class Preferences {

    protected static String PREF_TRACKEABLE_TYPE = "IS_TRACKEABLE_TYPE";
    protected static String PREF_STATUS_NETWORK = "IS_STATUS_NETWORK";
    protected static String PREF_LAST_TIME_NETWORK = "IS_LAST_TIME_NETWORK";
    protected static String PREF_ORGANIZATION_VALIDATE = "IS_PREF_ORGANIZATION_VALIDATE";
    protected static String PREF_INTERVAL_TIME_GPS = "IS_PREF_INTERVAL_TIME_GPS";
    protected static String PREF_GPS_LATITUDE = "PREF_GPS_LATITUDE";
    protected static String PREF_GPS_LONGITUDE = "PREF_GPS_LONGITUDE";

    SharedPreferences DB;

    public Preferences(Context ctx){
        DB = ctx.getSharedPreferences("sp_sgtareas", Context.MODE_PRIVATE);
    }


    protected void saveString(String key,String value){
        System.out.println("Guardando JMSTORE:"+key+"="+value);
        SharedPreferences.Editor editor = DB.edit();
        editor.putString(key, value);
        editor.apply();
    }

    protected String getString(String key) {
        if(DB.contains(key)) {
            return DB.getString(key, "");
        }
        return "";
    }

    protected void saveInt(String key,int value){
        System.out.println("Guardando JMSTORE:"+key+"="+value);
        SharedPreferences.Editor editor = DB.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    protected int getInt(String key) {
        if(DB.contains(key)) {
            return DB.getInt(key, 0);
        }
        return 0;
    }

    protected int nextInt() {
        int c=1;
        String ky = "next_val";
        if(DB.contains(ky))
            c += DB.getInt(ky,0);
        SharedPreferences.Editor editor = DB.edit();
        editor.putInt(ky, c);
        editor.apply();
        return c;
    }


    protected void pushBoolean(String key, boolean b) {
        SharedPreferences.Editor editor = DB.edit();
        editor.putBoolean(key, b);
        editor.apply();
    }

    protected boolean getBoolean(String key) {
        if(DB.contains(key)) {
            return DB.getBoolean(key, false);
        }
        return false;
    }
}