package com.jmt.sg.sgcontrol.data.datasource.rest.api;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.preferences.JMStore;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by JMTech-Android on 01/02/2016.
 */
public class InterceptorJM implements Interceptor{
    protected JMStore jmStore;
    protected Context context;

    public InterceptorJM(Context context){
        this.jmStore = new JMStore(context);
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        String uri = original.uri().toString();

//        if(!uri.endsWith("*"))  uri += "*";
        if(uri.endsWith("*")) return processMockService(chain,uri);

        System.out.println("Enviando para el Request--------->");
        System.out.println(jmStore.getString(S.VAR.COOKIE));
        System.out.println(S.VAR.Accept);
        System.out.println(S.VAR.PREF_TOKEN + " = " + jmStore.getString(S.VAR.TOKEN));
        System.out.println(" ------------------------- ");

        Request request = original.newBuilder()
                .header("Cookie", jmStore.getString(S.VAR.COOKIE))
                .addHeader("Accept", S.VAR.Accept)
                .addHeader("Authorization", S.VAR.PREF_TOKEN + " " + jmStore.getString(S.VAR.TOKEN))
                .method(original.method(), original.body())
                .build();
        return chain.proceed(request);
    }

    private Response processMockService(Chain chain,String uri) throws IOException {
        String responseString = "";
        if(!uri.endsWith("*"))
            uri += "*";
        if(uri.endsWith("today*")){
            responseString = getJson("data/tasks.json");
        }else if(uri.endsWith("searchTerm=*")){
            responseString = getJson("data/products.json");
        }else if(uri.endsWith("send_order*")){
            responseString = getJson("data/send_order.json");
        }else if(uri.endsWith("point*")){
            responseString = getJson("data/visit_point.json");
        }
        Response response = new Response.Builder()
                .code(200)
                .message(responseString)
                .request(chain.request())
                .protocol(Protocol.HTTP_1_0)
                .body(ResponseBody.create(MediaType.parse("application/json"), responseString.getBytes()))
                .addHeader("content-type", "application/json")
                .build();
        return response;
    }

    private String getJson(String filename) throws IOException {
        InputStream jsonF = context.getAssets().open(filename);
        StringBuilder buf=new StringBuilder();
        BufferedReader in= new BufferedReader(new InputStreamReader(jsonF, "UTF-8"));
        String str;
        while ((str=in.readLine()) != null) {
            buf.append(str);
        }
        jsonF.close();
        String responseString = buf.toString();
        return responseString;
    }
}