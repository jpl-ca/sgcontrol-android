package com.jmt.sg.sgcontrol.data.mapper;

import com.google.gson.Gson;
import com.jmt.sg.sgcontrol.data.model.FormToSendEntity;
import com.jmt.sg.sgcontrol.domain.model.FormToSendE;

/**
 * Created by jmtech on 5/16/16.
 */
public class SendFormDataMapper {
    Gson gson;

    public SendFormDataMapper(Gson gson){
        this.gson = gson;
    }

    public FormToSendEntity transformOrderToSend(FormToSendE objResponse){
        String str = gson.toJson(objResponse);
        FormToSendEntity objTransform = gson.fromJson(str,FormToSendEntity.class);
        return objTransform;
    }
}