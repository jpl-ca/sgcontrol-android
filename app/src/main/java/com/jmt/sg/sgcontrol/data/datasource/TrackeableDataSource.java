package com.jmt.sg.sgcontrol.data.datasource;

import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/13/16.
 */
public interface TrackeableDataSource {
    void saveAgent(AgentTrackeable agentTrackeable, RepositoryCallback repositoryCallback);

    void getAgent(RepositoryCallback repositoryCallback);

    void removeAgent(RepositoryCallback repositoryCallback);
}