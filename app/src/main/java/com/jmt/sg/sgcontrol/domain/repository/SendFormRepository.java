package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.domain.interactor.SendFormInteractor;
import com.jmt.sg.sgcontrol.domain.model.FormToSendE;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SendFormRepository {
    void sendForm(long visitPointId, FormToSendE data, final SendFormInteractor.Callback sCallback);
}