package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgcontrol.domain.interactor.TrackerLoadInteractor;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.data.datasource.TrackeableDataSource;
import com.jmt.sg.sgcontrol.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.domain.repository.TrackerLoadDataRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class TrackerDataLoadRepository implements TrackerLoadDataRepository {
    private final TrackeableDataSourceFactory trackeableDataSourceFactory;
    private final TrackeableDataMapper trackeableDataMapper;

    public TrackerDataLoadRepository(TrackeableDataSourceFactory trackeableDataSourceFactory, TrackeableDataMapper trackeableDataMapper) {
        this.trackeableDataSourceFactory = trackeableDataSourceFactory;
        this.trackeableDataMapper= trackeableDataMapper;
    }

    @Override
    public void loadAgentData(final TrackerLoadInteractor.Callback aCallback) {
            TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
            trackeableDataSource.getAgent(new RepositoryCallback() {
                @Override
                public void onSuccess(Object object) {
                    AgentTrackeable at = (AgentTrackeable)object;
                    aCallback.onLoadAgentData(at);
                }

                @Override
                public void onError(Object object) {
                    String mess = object.toString();
                    aCallback.onLoadDataError(mess);
                }
            });
    }
}