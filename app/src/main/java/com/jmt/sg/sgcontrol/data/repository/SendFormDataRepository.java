package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.datasource.SendFormDataSource;
import com.jmt.sg.sgcontrol.data.datasource.SendFormDataSourceFactory;
import com.jmt.sg.sgcontrol.data.mapper.SendFormDataMapper;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.data.model.FormToSendEntity;
import com.jmt.sg.sgcontrol.domain.interactor.SendFormInteractor;
import com.jmt.sg.sgcontrol.domain.model.FormToSendE;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.domain.repository.SendFormRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class SendFormDataRepository implements SendFormRepository {

    private static final String TAG = "LoginDataRepository";
    private final SendFormDataSourceFactory sendFormDataSourceFactory;
    private final SendFormDataMapper sendFormDataMapper;

    public SendFormDataRepository(SendFormDataSourceFactory sendFormDataSourceFactory, SendFormDataMapper sendFormDataMapper) {
        this.sendFormDataSourceFactory = sendFormDataSourceFactory;
        this.sendFormDataMapper = sendFormDataMapper;
    }

    @Override
    public void sendForm(long visitPointId, FormToSendE formToSendE, final SendFormInteractor.Callback sCallback) {
        FormToSendEntity formToSendEntity = sendFormDataMapper.transformOrderToSend(formToSendE);

        SendFormDataSource sendFormDataSource = sendFormDataSourceFactory.create(DataSourceFactory.CLOUD);
        sendFormDataSource.sendForm(visitPointId, formToSendEntity, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                sCallback.onSendFormSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null)
                    message= object.toString();
                System.out.println(TAG+"->"+message);
                sCallback.onSendFormError(message);
            }
        });
    }
}