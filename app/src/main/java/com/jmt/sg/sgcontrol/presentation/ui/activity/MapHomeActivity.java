package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.MapHomeFragment;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.TimelineFragment;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.VisitOrderFragment;

import java.util.ArrayList;

public class MapHomeActivity extends BaseActivity implements MapHomeFragment.OnHomeMapListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_home);
    }

    @Override
    public void profileActivity() {
        Intent it = new Intent(this,ProfileActivity.class);
        startActivity(it);
    }

    @Override
    public void visitDetailActivity(RouteVisitE routeVisit) {
        Intent it = new Intent(this,VisitOrderActivity.class);
        it.putExtra(VisitOrderFragment.ROUTE_VISIT_DATA,routeVisit);
        startActivityForResult(it,VisitOrderActivity.CODE_RESULT);
    }

    @Override
    public void timeLineActivity(ArrayList<RouteVisitE> routeVisits) {
        Intent it = new Intent(this,TimelineActivity.class);
        it.putExtra(TimelineFragment.ROUTE_VISITS_DATA,routeVisits);
        startActivityForResult(it,VisitOrderActivity.CODE_RESULT);
    }
}