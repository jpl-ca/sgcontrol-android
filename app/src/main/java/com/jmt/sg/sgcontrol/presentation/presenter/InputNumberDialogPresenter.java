package com.jmt.sg.sgcontrol.presentation.presenter;

import com.jmt.sg.sgcontrol.presentation.ui.dialog.InputNumberDialog;
import com.jmt.sg.sgcontrol.presentation.ui.dialog.callback.CallbackInputNumberDialog;
import com.jmt.sg.sgcontrol.presentation.view.InputNumberDialogView;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public class InputNumberDialogPresenter {
    private InputNumberDialogView delegate;
    private InputNumberDialog inputNumberDialog;

    public InputNumberDialogPresenter(InputNumberDialogView delegate) {
        this.delegate = delegate;
        inputNumberDialog = new InputNumberDialog(delegate.getContext());
    }

    public void showInputNumberDialog(int value) {
        inputNumberDialog.showInputNumberDialog(value, new CallbackInputNumberDialog() {
            @Override
            public void confirm(int input) {
                delegate.clickConfirm(input);
            }
            @Override
            public void cancel() {
                delegate.clickCancel();
            }
        });
    }
}