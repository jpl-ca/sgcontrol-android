package com.jmt.sg.sgcontrol.presentation.view;

import android.content.Context;

/**
 * Created by JMTech-Android on 15/10/2015.
 */
public interface InputNumberDialogView {
    void clickConfirm(int input);
    void clickCancel();
    Context getContext();
}