package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.repository.RescheduleVisitRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class RescheduleVisitInteractor {
    private final RescheduleVisitRepository rescheduleVisitRepository;

    public RescheduleVisitInteractor(RescheduleVisitRepository rescheduleVisitRepository) {
        this.rescheduleVisitRepository = rescheduleVisitRepository;
    }

    public void rescheduleVisitPoint(final long visit_point_id,String comment, Callback callback){
        rescheduleVisitRepository.rescheduleVisit(visit_point_id,comment,callback);
    }

    public interface Callback {
        void onRescheduleSuccess();
        void onRescheduleError(String message);
    }
}