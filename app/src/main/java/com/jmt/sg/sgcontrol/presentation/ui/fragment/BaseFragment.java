package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;

/**
 * Created by jmtech on 5/12/16.
 */
public class BaseFragment extends Fragment {
    AppCompatActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity) getActivity();
    }

    protected void injectView(View view) {
        ButterKnife.bind(this,view);
    }

    protected void setToolbar(Toolbar toolbar, String title){
        toolbar.setTitle(title);
        activity.setSupportActionBar(toolbar);
    }

    public AppCompatActivity getAppActivity(){
        return activity;
    }

    protected void showMessage(View container, String message) {
        Snackbar snackbar = Snackbar.make(container,message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}