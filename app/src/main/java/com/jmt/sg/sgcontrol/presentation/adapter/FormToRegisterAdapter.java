package com.jmt.sg.sgcontrol.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.FormValuesE;
import com.jmt.sg.sgcontrol.presentation.utils.Constants;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/20/16.
 */
public class FormToRegisterAdapter extends RecyclerView.Adapter<FormToRegisterAdapter.ViewHolder> {
    private Context ctx;
    private ItemsListener itemsListener;
    private ArrayList<FormValuesE> data;
    public FormToRegisterAdapter(Context _ctx, ArrayList<FormValuesE> data, ItemsListener itemsListener){
        ctx = _ctx;
        this.data = data;
        this.itemsListener = itemsListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_form_to_register, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        FormValuesE obj = data.get(position);
        holder.tv_nombre.setText(obj.getLabel());
        holder.tv_description.setText(obj.getDescription());
        holder.tv_value.setHint(obj.getLabel());
        if(obj.getValue()!=null)
            holder.tv_value.setText(obj.getValue());
        if(obj.getType()!=null&&obj.getType().equals(Constants.TYPE_INPUT_TEXT.Integer))
            holder.tv_value.setInputType(InputType.TYPE_CLASS_NUMBER);
        holder.tv_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                itemsListener.onFormValueChanged(position,s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_nombre,tv_description;
        private EditText tv_value;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_nombre = (TextView)itemView.findViewById(R.id.tv_name);
            tv_description = (TextView)itemView.findViewById(R.id.tv_description);
            tv_value = (EditText)itemView.findViewById(R.id.tv_value);
        }
    }

    public interface ItemsListener{
        void onFormValueChanged(int pos,String value);
    }
}