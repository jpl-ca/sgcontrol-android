package com.jmt.sg.sgcontrol.data.model;

import com.jmt.sg.sgcontrol.data.mapper.BaseResponse;

/**
 * Created by jmtech on 5/16/16.
 */
public class ValidateOrganizationResponse extends BaseResponse {
    private String message;
    private OrganizationEntity data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrganizationEntity getData() {
        return data;
    }

    public void setData(OrganizationEntity data) {
        this.data = data;
    }
}