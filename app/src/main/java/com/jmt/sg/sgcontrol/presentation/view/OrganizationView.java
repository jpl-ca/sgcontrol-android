package com.jmt.sg.sgcontrol.presentation.view;

/**
 * Created by jmtech on 5/12/16.
 */
public interface OrganizationView extends BaseView {
    void showLoading();
    void hideLoading();
    void successValidate();
    void showErrorMessage(String message);
}