package com.jmt.sg.sgcontrol.data.datasource.db.realm;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.db.rx.RealmObservable;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.data.datasource.db.entity.AgentTrackeableRealm;

import io.realm.Realm;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by jmtech on 5/16/16.
 */
public class AgentDataService implements AgentDataServiceI{
    private final Context context;

    public AgentDataService(Context context){
        this.context = context;
    }

    @Override
    public Observable<Boolean> registerAgent(final AgentTrackeable user) {
        return RealmObservable.object(context, new Func1<Realm, AgentTrackeableRealm>() {
            @Override
            public AgentTrackeableRealm call(Realm realm) {
                AgentTrackeableRealm agentR = new AgentTrackeableRealm();
                agentR.setId(user.getId());
                agentR.setFirst_name(user.getFirst_name());
                agentR.setLast_name(user.getLast_name());
                agentR.setType(user.getType());
                agentR.setToken(user.getToken());
                agentR.setOrganization_id(user.getOrganization_id());
                agentR.setAuthentication_code(user.getAuthentication_code());
                agentR.setLocation_frequency(user.getLocation_frequency());
                agentR = realm.copyToRealmOrUpdate(agentR);
                return agentR;
            }
        }).map(new Func1<AgentTrackeableRealm, Boolean>() {
            @Override
            public Boolean call(AgentTrackeableRealm agentR) {
                return agentR != null;
            }
        });
    }

    @Override
    public Observable<AgentTrackeable> getAgent() {
        return RealmObservable.object(context, new Func1<Realm, AgentTrackeableRealm>() {
            @Override
            public AgentTrackeableRealm call(Realm realm) {
                AgentTrackeableRealm agentR = realm.where(AgentTrackeableRealm.class).findFirst();
                System.out.println("NEW AG::::"+agentR.getFirst_name());
                return agentR;
            }
        }).map(new Func1<AgentTrackeableRealm, AgentTrackeable>() {
            @Override
            public AgentTrackeable call(AgentTrackeableRealm realmUser) {
                AgentTrackeable user = new AgentTrackeable();
                user.setId(realmUser.getId());
                user.setFirst_name(realmUser.getFirst_name());
                user.setLast_name(realmUser.getLast_name());
                user.setType(realmUser.getType());
                user.setToken(realmUser.getToken());
                user.setOrganization_id(realmUser.getOrganization_id());
                user.setAuthentication_code(realmUser.getAuthentication_code());
                user.setLocation_frequency(realmUser.getLocation_frequency());
                return user;
            }
        });
    }

    @Override
    public Observable<Boolean> removeAgent() {
        return RealmObservable.object(context, new Func1<Realm, AgentTrackeableRealm>() {
            @Override
            public AgentTrackeableRealm call(Realm realm) {
                AgentTrackeableRealm us = realm.where(AgentTrackeableRealm.class).findFirst();
                realm.where(AgentTrackeableRealm.class).findAll().clear();
                return us;
            }
        }).map(new Func1<AgentTrackeableRealm, Boolean>() {
            @Override
            public Boolean call(AgentTrackeableRealm realmUser) {
                return true;
            }
        });
    }
}
