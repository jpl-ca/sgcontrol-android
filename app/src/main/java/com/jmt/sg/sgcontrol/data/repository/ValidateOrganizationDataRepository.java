package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.datasource.ValidateOrganizationDataSource;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.data.datasource.ValidateOrganizationDataSourceFactory;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.domain.interactor.OrganizationInteractor;
import com.jmt.sg.sgcontrol.domain.repository.OrganizationRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class ValidateOrganizationDataRepository implements OrganizationRepository {

    private static final String TAG = "ValidateOrganizationDataRepository";
    private final ValidateOrganizationDataSourceFactory validateOrganizationDataSourceFactory;

    public ValidateOrganizationDataRepository(ValidateOrganizationDataSourceFactory validateOrganizationDataSourceFactory) {
        this.validateOrganizationDataSourceFactory = validateOrganizationDataSourceFactory;
    }

    @Override
    public void validateOrganization(final String organization_name, final OrganizationInteractor.Callback organizationInteractorCallback) {
        ValidateOrganizationDataSource validateOrganizationDataSource = validateOrganizationDataSourceFactory.create(DataSourceFactory.CLOUD);
        validateOrganizationDataSource.validateOrganization(organization_name, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                organizationInteractorCallback.onValidateSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                organizationInteractorCallback.onValidateError(message);
            }
        });
    }
}