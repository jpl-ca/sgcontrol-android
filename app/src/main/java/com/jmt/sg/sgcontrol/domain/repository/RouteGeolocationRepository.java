package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.domain.interactor.RouteGeolocationInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RouteGeolocationRepository {
    void storeGeolocation(double lat, double lng, final RouteGeolocationInteractor.Callback rCallback);
}