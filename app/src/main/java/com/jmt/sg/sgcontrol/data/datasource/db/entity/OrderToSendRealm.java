package com.jmt.sg.sgcontrol.data.datasource.db.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jmtech on 5/16/16.
 */
public class OrderToSendRealm extends RealmObject {
    @PrimaryKey
    private long id;

    private long visit_id;

    private ItemInOrderRealm itemInOrder;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getVisit_id() {
        return visit_id;
    }

    public void setVisit_id(long visit_id) {
        this.visit_id = visit_id;
    }

    public ItemInOrderRealm getItemInOrder() {
        return itemInOrder;
    }

    public void setItemInOrder(ItemInOrderRealm itemInOrder) {
        this.itemInOrder = itemInOrder;
    }
}