package com.jmt.sg.sgcontrol.domain.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class FormE implements Serializable {
    private long id;

    private long form_template_id;

    private String description;

    private String status;

    private ArrayList<FormValuesE> form_values;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getForm_template_id() {
        return form_template_id;
    }

    public void setForm_template_id(long form_template_id) {
        this.form_template_id = form_template_id;
    }

    public ArrayList<FormValuesE> getForm_values() {
        return form_values;
    }

    public void setForm_values(ArrayList<FormValuesE> form_values) {
        this.form_values = form_values;
    }
}