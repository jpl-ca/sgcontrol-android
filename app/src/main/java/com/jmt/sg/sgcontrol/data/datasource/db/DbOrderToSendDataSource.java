package com.jmt.sg.sgcontrol.data.datasource.db;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.OrderToSendDataSource;
import com.jmt.sg.sgcontrol.data.datasource.db.realm.OrderToSendDataService;
import com.jmt.sg.sgcontrol.data.datasource.db.realm.OrderToSendDataServiceI;
import com.jmt.sg.sgcontrol.data.model.FormToSendEntity;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by jmtech on 5/13/16.
 */
public class DbOrderToSendDataSource implements OrderToSendDataSource {

    private CompositeSubscription compositeSubscription;
    private OrderToSendDataServiceI orderToSendDataServiceI;
    private final Context context;

    public DbOrderToSendDataSource(Context context) {
        this.context = context;
        orderToSendDataServiceI = new OrderToSendDataService(context);
        compositeSubscription = new CompositeSubscription();
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    @Override
    public void saveOrderToSend(long visit_id, FormToSendEntity user, final RepositoryCallback repositoryCallback) {
        Subscription subscription = orderToSendDataServiceI.registerOrderToSend(visit_id, user).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean success) {
                                System.out.println("Agent fue insertado...>"+success);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    @Override
    public void getOrderToSend(long visit_id, final RepositoryCallback repositoryCallback) {
        Subscription subscription = orderToSendDataServiceI.getOrderToSend(visit_id).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<FormToSendEntity>() {
                            @Override
                            public void call(FormToSendEntity track) {
                                repositoryCallback.onSuccess(track);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    @Override
    public void removeOrderToSend(long visit_id, final RepositoryCallback repositoryCallback) {
        Subscription subscription = orderToSendDataServiceI.removeOrderToSend(visit_id).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean track) {
                                repositoryCallback.onSuccess(track);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }
}