package com.jmt.sg.sgcontrol.data.datasource.rest.service;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.rest.api.ApiService;

import org.json.JSONObject;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class GeoLocationService extends ApiService{
    private GeoLocationApi apiService;

    public GeoLocationService(Context context) {
        super(context);
        apiService = retrofit.create(GeoLocationApi.class);
    }

    public GeoLocationApi getApi() {
        return apiService;
    }

    public interface GeoLocationApi{
        @FormUrlEncoded
        @POST(_api+"/auth/me/store-geolocation")
        Observable<JSONObject> storeGeolocation(@Field("lat") double lat,@Field("lng") double lng);

        @FormUrlEncoded
        @POST(_api+"/auth/me/store-geolocation")
        Observable<JSONObject> storeGeolocation(@Field("task_id") long task_id,@Field("lat") double lat,@Field("lng") double lng);
    }
}