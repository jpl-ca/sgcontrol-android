package com.jmt.sg.sgcontrol.data.model;

import com.jmt.sg.sgcontrol.data.mapper.BaseResponse;

/**
 * Created by jmtech on 5/16/16.
 */
public class RouteVisitResponse extends BaseResponse {
    private long id;

    private String name;

    private double lat;

    private double lng;

    private String address;

    private String phone;

    private String datetime;

    private String time;

    private String reference;

    private int checklist_items;

    private int checklist_done_items;

    private int visit_state_id;

    private FormResponse form;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FormResponse getForm() {
        return form;
    }

    public void setForm(FormResponse form) {
        this.form = form;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getChecklist_items() {
        return checklist_items;
    }

    public void setChecklist_items(int checklist_items) {
        this.checklist_items = checklist_items;
    }

    public int getChecklist_done_items() {
        return checklist_done_items;
    }

    public void setChecklist_done_items(int checklist_done_items) {
        this.checklist_done_items = checklist_done_items;
    }

    public int getVisit_state_id() {
        return visit_state_id;
    }

    public void setVisit_state_id(int visit_state_id) {
        this.visit_state_id = visit_state_id;
    }
}