package com.jmt.sg.sgcontrol.data.datasource;

import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.data.model.FormToSendEntity;

/**
 * Created by jmtech on 5/13/16.
 */
public interface SendFormDataSource {
    void sendForm(long visitPointId, FormToSendEntity data, RepositoryCallback repositoryCallback);
}