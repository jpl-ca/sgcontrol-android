package com.jmt.sg.sgcontrol.presentation.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgcontrol.domain.model.FormValuesE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RegisterFormView extends BaseView {
    Context getContext();
    AppCompatActivity getAppActivity();
    void showFormsList(ArrayList<FormValuesE> items);
    void showMessage(String message);
    void registerFromSent(ArrayList<FormValuesE> form_fields);
}