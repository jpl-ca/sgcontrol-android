package com.jmt.sg.sgcontrol.data.model;

import java.io.Serializable;

/**
 * Created by JMTech-Android on 05/01/2016.
 */
public class AgentTrackeableEntity implements Serializable{

    private int id;


    /** Agent*/

    private String authentication_code;

    private String first_name;

    private String last_name;


    /** Common*/

    private String organization_id;

    private String type;

    private String token;

    private int location_frequency;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthentication_code() {
        return authentication_code;
    }

    public void setAuthentication_code(String authentication_code) {
        this.authentication_code = authentication_code;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getLocation_frequency() {
        return location_frequency;
    }

    public void setLocation_frequency(int location_frequency) {
        this.location_frequency = location_frequency;
    }
}