package com.jmt.sg.sgcontrol.presentation.presenter;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.data.datasource.RescheduleVisitDataSourceFactory;
import com.jmt.sg.sgcontrol.data.repository.RescheduleVisitDataRepository;
import com.jmt.sg.sgcontrol.domain.interactor.RescheduleVisitInteractor;
import com.jmt.sg.sgcontrol.domain.interactor.SendFormInteractor;
import com.jmt.sg.sgcontrol.domain.model.FormE;
import com.jmt.sg.sgcontrol.domain.model.FormValuesE;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.domain.repository.RescheduleVisitRepository;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.VisitOrderFragment;
import com.jmt.sg.sgcontrol.presentation.utils.Constants;
import com.jmt.sg.sgcontrol.presentation.view.VisitOrderView;
import com.jmt.sg.sgcontrol.tracking.invoker.GpsInvoker;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public class VisitOrderPresenter implements Presenter<VisitOrderView>{
    private Context context;
    private VisitOrderView visitOrderView;

    private RouteVisitE routeVisit;
    private GpsInvoker gpsInvoker;

    private RescheduleVisitInteractor rescheduleVisitInteractor;

    public VisitOrderPresenter(){
        routeVisit = new RouteVisitE();
    }

    @Override
    public void addView(VisitOrderView view) {
        visitOrderView = view;
        context = view.getContext();

        RescheduleVisitRepository rescheduleVisitRepository = new RescheduleVisitDataRepository(new RescheduleVisitDataSourceFactory(context));
        rescheduleVisitInteractor = new RescheduleVisitInteractor(rescheduleVisitRepository);

        routeVisit = (RouteVisitE) visitOrderView.getAppActivity().getIntent().getSerializableExtra(VisitOrderFragment.ROUTE_VISIT_DATA);
        System.out.println("VISIT:-->"+new Gson().toJson(routeVisit));

        visitOrderView.settingData(routeVisit);
        gpsInvoker = new GpsInvoker(context);

        visitOrderView.setVisitPointState(routeVisit.getVisit_state_id());
    }

    @Override
    public void removeView() {
        visitOrderView = null;
    }

    public RouteVisitE getRouteVisit() {
        return routeVisit;
    }

    public void getMyLocation(final onGetMyLocation callback) {
        gpsInvoker.startGpsData();
        gpsInvoker.startGpsListener(new GpsInvoker.LocationCallback() {
            @Override
            public void updateLocation(Location location) {
                callback.setMyLocation(new LatLng(location.getLatitude(),location.getLongitude()));
                gpsInvoker.stopGpsListener();
                gpsInvoker.stopGpsData();
            }
        });
    }

    public FormE getFormToResponse() {
        return routeVisit.getForm();
    }

    public void rescheduleVisitPoint(String comment) {
        final long visit_point_id = routeVisit.getId();
        routeVisit.setVisit_state_id(Constants.VISIT_STATE.ReScheduling);
        rescheduleVisitInteractor.rescheduleVisitPoint(visit_point_id, comment, new RescheduleVisitInteractor.Callback() {
            @Override
            public void onRescheduleSuccess() {
                visitOrderView.visitPointRescheduled();
                visitOrderView.setVisitPointState(Constants.VISIT_STATE.ReScheduling);
            }

            @Override
            public void onRescheduleError(String message) {
            }
        });
    }

    public void refreshRouteForm(ArrayList<FormValuesE> form_values) {
        routeVisit.getForm().setForm_values(form_values);
    }

    public interface onGetMyLocation{
        void setMyLocation(LatLng ll);
    }
}