package com.jmt.sg.sgcontrol.data.datasource.preferences;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.LoginDataSource;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public class PreferencesLoginDataSource implements LoginDataSource {

    private final Context context;
    public PreferencesLoginDataSource(Context context) {
        this.context= context;
    }

    @Override
    public void loginAgent(String identification_code, String password, RepositoryCallback repositoryCallback) {

    }
}