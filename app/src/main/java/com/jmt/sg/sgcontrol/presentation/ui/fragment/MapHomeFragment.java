package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.presentation.presenter.HomeMapPresenter;
import com.jmt.sg.sgcontrol.presentation.presenter.TrackeablePresenter;
import com.jmt.sg.sgcontrol.presentation.view.HomeMapView;
import com.jmt.sg.sgcontrol.presentation.view.TrackeableView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindString;

/**
 * A placeholder fragment containing a simple view.
 */
public class MapHomeFragment extends BaseFragment implements TrackeableView, HomeMapView, OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,GoogleMap.OnMarkerClickListener {
    @BindString(R.string.t_inicio) protected String titlebar;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.ll_root) RelativeLayout ll_root;

    private OnHomeMapListener presenter;
    private TrackeablePresenter trackeablePresenter;
    private HomeMapPresenter homeMapPresenter;

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private float ZOOM_MAP = 13;
    private boolean FRAG_CREATED = false;

    public static MapHomeFragment instance() {
        return new MapHomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map_home, container, false);
        injectView(rootView);

        trackeablePresenter = new TrackeablePresenter();
        trackeablePresenter.addView(this);

        homeMapPresenter = new HomeMapPresenter();
        homeMapPresenter.addView(this);

        setToolbar(toolbar, titlebar);
        toolbar.setNavigationIcon(R.mipmap.ic_bar);
        setHasOptionsMenu(true);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        trackeablePresenter.getTrackeableInfo();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(FRAG_CREATED){
            System.out.println("----------->>>>Recargando datos de Ruta.....!!!!!!");
            homeMapPresenter.getRouteVisit();
        }

        FRAG_CREATED = true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHomeMapListener) {
            presenter = (OnHomeMapListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        homeMapPresenter.stopUpdateLocations();
        trackeablePresenter.removeView();
        homeMapPresenter.removeView();
    }

    @Override
    public void showData(AgentTrackeable agentTrackeable) {
        setToolbar(toolbar, agentTrackeable.getFirst_name() + " " + agentTrackeable.getLast_name());
    }

    @Override
    public void showErrorMessage(String message) {
        showMessage(ll_root, message);
    }

    @Override
    public void updatePosition(final LatLng ll) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, mMap.getCameraPosition().zoom));
            }
        }, 50);
    }

    @Override
    public void clearMap() {
        mMap.clear();
    }

    @Override
    public void firstPosition(final LatLng ll) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, ZOOM_MAP));
                homeMapPresenter.startUpdateLocations();
            }
        }, 350);
    }

    @Override
    public Marker addVisitMarker(MarkerOptions markerOptions) {
        return mMap.addMarker(markerOptions);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        RouteVisitE routeVisitE = homeMapPresenter.onVisitClick(marker.getId());
        if(routeVisitE != null){
            presenter.visitDetailActivity(routeVisitE);
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_profile:presenter.profileActivity();break;
            case R.id.action_timeline:startTimeline();break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startTimeline() {
        ArrayList<RouteVisitE> visits = homeMapPresenter.getRouteVisits();
        presenter.timeLineActivity(visits);
    }

    public interface OnHomeMapListener {
        void visitDetailActivity(RouteVisitE routeVisit);
        void timeLineActivity(ArrayList<RouteVisitE> routeVisits);
        void profileActivity();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(this);

        System.out.println("----------->>>>Cargando datos de Ruta.....!!!!!!");
        homeMapPresenter.getRouteVisit();

        homeMapPresenter.startGpsData();
        homeMapPresenter.getLastLocation();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

}