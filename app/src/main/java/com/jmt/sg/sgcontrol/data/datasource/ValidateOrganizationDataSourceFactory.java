package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.db.DbOrganizationDataSource;
import com.jmt.sg.sgcontrol.data.datasource.preferences.PreferencesOrganizationDataSource;
import com.jmt.sg.sgcontrol.data.datasource.rest.RestOrganizationDataSource;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class ValidateOrganizationDataSourceFactory {
    private final Context context;

    public ValidateOrganizationDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public ValidateOrganizationDataSource create(DataSourceFactory dataSourceFactory) {

        ValidateOrganizationDataSource placeDataStore = null;
        switch (dataSourceFactory) {
            case CLOUD:
                placeDataStore = new RestOrganizationDataSource(context);
                break;
            case DB:
                placeDataStore = new DbOrganizationDataSource(context);
                break;
            case PREFERENCES:
                placeDataStore = new PreferencesOrganizationDataSource(context);
                break;
        }
        return placeDataStore;
    }
}