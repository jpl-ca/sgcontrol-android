package com.jmt.sg.sgcontrol.presentation.utils;

/**
 * Created by jmtech on 5/26/16.
 */
public final class Constants {
    public static final class VISIT_STATE{
        public static final String VisitState = "VISIT_STATE";
        public static final int Scheduled = 1;
        public static final int Done = 2;
        public static final int ReScheduling = 3;
        public static final int Cancelled = 4;
    }
    public static final class CHECKED_LIST_STATE{
        public static final int Unchecked= 0;
        public static final int Checked = 1;
    }
    public static final class TYPE_INPUT_TEXT{
        public static final String Integer = "integer";
        public static final String String = "string";
    }
}