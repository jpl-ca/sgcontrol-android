package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.repository.SessionRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class SessionInteractor {
    private final SessionRepository sessionRepository;

    public SessionInteractor(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public void checkSession(final CheckSessionCallback sessionCallback) {
        sessionRepository.checkSesion(sessionCallback);
    }

    public void closeSession(final CloseSessionCallback logoutCallback) {
        sessionRepository.closeSesion(logoutCallback);
    }

    public interface CheckSessionCallback {
        void onValidateSuccess();
        void onValidateError(String message);
    }

    public interface CloseSessionCallback {
        void onLogoutSuccess();
        void onLogoutError(String message);
    }
}