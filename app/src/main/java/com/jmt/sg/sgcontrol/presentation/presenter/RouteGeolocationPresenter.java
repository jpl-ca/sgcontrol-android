package com.jmt.sg.sgcontrol.presentation.presenter;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.repository.RouteGeolocationDataRepository;
import com.jmt.sg.sgcontrol.data.datasource.RouteGeolocationDataSourceFactory;
import com.jmt.sg.sgcontrol.domain.interactor.RouteGeolocationInteractor;
import com.jmt.sg.sgcontrol.presentation.view.RouteGeolocationView;

/**
 * Created by jmtech on 5/12/16.
 */
public class RouteGeolocationPresenter implements Presenter<RouteGeolocationView>, RouteGeolocationInteractor.Callback {

    Context context;
    RouteGeolocationView routeGeolocationView;
    private RouteGeolocationInteractor routeGeolocationInteractor;

    @Override
    public void addView(RouteGeolocationView view) {
        routeGeolocationView = view;
        context = routeGeolocationView.getContext();

        RouteGeolocationDataRepository routeGeolocationDataRepository = new RouteGeolocationDataRepository(new RouteGeolocationDataSourceFactory(context));
        routeGeolocationInteractor = new RouteGeolocationInteractor(routeGeolocationDataRepository);
    }

    @Override
    public void removeView() {
        routeGeolocationView = null;
    }

    public void registerRouteGeolocation(double lat, double lng){
        routeGeolocationInteractor.storeGeolocation(lat,lng,this);
    }

    @Override
    public void onRegisterGeolocationSuccess() {

    }

    @Override
    public void onRegisterGeolocationError(String message) {

    }
}