package com.jmt.sg.sgcontrol.data.datasource.preferences;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.SGInfoDataSource;
import com.jmt.sg.sgcontrol.data.model.Trackeable;

/**
 * Created by JMTech-Android on 22/01/2016.
 */


public class PreferencesSGInfoDataSource extends Preferences implements SGInfoDataSource {
    private final int NONE = 0;
    private final int AGENT = 1;
    private final int VEHICLE = 2;

    public PreferencesSGInfoDataSource(Context ctx){
        super(ctx);
    }

    @Override
    public void saveTrackeableType(Trackeable trackeable){
        int type = 0;
        switch (trackeable){
            case AGENT: type = AGENT; break;
            case VEHICLE: type = VEHICLE; break;
            default: type = NONE;
        }
        saveInt(PREF_TRACKEABLE_TYPE,type);
    }

    @Override
    public Trackeable getTrackeableType(){
        int type = getInt(PREF_TRACKEABLE_TYPE);
        Trackeable t = Trackeable.NONE;
        switch (type){
            case AGENT: t =Trackeable.AGENT; break;
            case VEHICLE: t= Trackeable.VEHICLE; break;
        }
        return t;
    }
}