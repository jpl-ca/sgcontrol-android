package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.presenter.LoginPresenter;
import com.jmt.sg.sgcontrol.presentation.view.LoginView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends BaseFragment implements LoginView {
    @Bind(R.id.txt_id) AppCompatEditText txt_email;
    @Bind(R.id.txt_password) AppCompatEditText txt_password;
    @Bind(R.id.btn_ingresar) Button btn_ingresar;
    @Bind(R.id.ll_root) LinearLayout ll_root;

    private OnLoginListener presenter;
    private LoginPresenter loginPresenter;

    public static LoginFragment instance(){
        return new LoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        injectView(rootView);

        loginPresenter = new LoginPresenter();
        loginPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnLoginListener) {
            presenter = (OnLoginListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @OnClick(R.id.btn_ingresar)
    public void login(){
        String email = txt_email.getText().toString();
        String password = txt_password.getText().toString();
        loginPresenter.login(email, password);
    }

    @OnClick(R.id.btn_cambiar_organizacion)
    public void cambiarOrganizacion(){
        presenter.validateOrganizationActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        loginPresenter.removeView();
    }


    @Override
    public void showLoading() {
        txt_email.setEnabled(false);
        txt_password.setEnabled(false);
        btn_ingresar.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        txt_email.setEnabled(true);
        txt_password.setEnabled(true);
        btn_ingresar.setEnabled(true);
    }

    @Override
    public void successLogin() {
        presenter.nextActivity();
    }

    @Override
    public void showErrorMessage(String message) {
        showMessage(ll_root,message);
    }

    public interface OnLoginListener{
        void nextActivity();
        void validateOrganizationActivity();
    }
}