package com.jmt.sg.sgcontrol.data.datasource.rest.api;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.preferences.JMStore;

import java.io.IOException;
import java.net.CookieManager;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Created by JMTech-Android on 12/01/2016.
 */
public class JMCookieManager extends CookieManager {
    JMStore jmStore;
    public JMCookieManager(Context context){
        this.jmStore = new JMStore(context);
    }
    @Override
    public void put(URI uri, Map<String, List<String>> stringListMap) throws IOException {
        super.put(uri, stringListMap);
        if (stringListMap != null && stringListMap.get(S.VAR.SET_COOKIE) != null)
            for (String strCK : stringListMap.get(S.VAR.SET_COOKIE)) {
                jmStore.saveString(S.VAR.COOKIE, strCK);
            }
    }
}