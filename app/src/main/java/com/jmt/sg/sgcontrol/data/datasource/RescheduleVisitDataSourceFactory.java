package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.rest.RestRescheduleVisitDataSource;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class RescheduleVisitDataSourceFactory {
    private final Context context;

    public RescheduleVisitDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public RescheduleVisitDataSource create(DataSourceFactory dataSource) {
        RescheduleVisitDataSource rescheduleVisitDataSource= null;
        switch (dataSource) {
            case CLOUD:
                rescheduleVisitDataSource = new RestRescheduleVisitDataSource(context);
                break;
            case DB:
//                rescheduleVisitDataSource = new DbLoginDataSource(context);
                break;
        }
        return rescheduleVisitDataSource;
    }
}