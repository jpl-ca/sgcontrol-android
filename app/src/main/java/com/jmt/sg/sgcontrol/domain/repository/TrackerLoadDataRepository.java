package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.domain.interactor.TrackerLoadInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackerLoadDataRepository {
    void loadAgentData(TrackerLoadInteractor.Callback aCallback);
}