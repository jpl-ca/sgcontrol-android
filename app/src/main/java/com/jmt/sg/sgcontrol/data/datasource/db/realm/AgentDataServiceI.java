package com.jmt.sg.sgcontrol.data.datasource.db.realm;

import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;

import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface AgentDataServiceI {
    Observable<AgentTrackeable> getAgent();
    Observable<Boolean> registerAgent(AgentTrackeable user);
    Observable<Boolean> removeAgent();
}