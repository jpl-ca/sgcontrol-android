package com.jmt.sg.sgcontrol.data.datasource.rest.api;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jmt.sg.sgcontrol.data.datasource.preferences.JMStore;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.concurrent.TimeUnit;

import io.realm.RealmObject;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class ApiService {
    protected Retrofit retrofit;
    protected Context context;
    protected JMStore jmStore;

    protected static final String _api = "/api";

    public ApiService(Context context) {
        this.context = context;
        jmStore = new JMStore(context);
        Gson gson = new GsonBuilder().setExclusionStrategies(new  ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getDeclaringClass().equals(RealmObject.class);
            }
            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
            })
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

        retrofit = new Retrofit.Builder()
            .baseUrl(S.VAR.SERVER_URL)
            .client(getClient(context))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build();
    }

    private static OkHttpClient getClient(Context context) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient();
        client.setCookieHandler(new JMCookieManager(context));
        client.interceptors().add(logging);
        client.interceptors().add(new InterceptorJM(context));

        client.setConnectTimeout(2, TimeUnit.MINUTES);
        client.setReadTimeout(2, TimeUnit.MINUTES);
        return client;
    }
}