package com.jmt.sg.sgcontrol.data.model;

import com.jmt.sg.sgcontrol.data.mapper.BaseResponse;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class SearchItemResponse extends BaseResponse {
    private ArrayList<ItemProductResponse> data;

    public ArrayList<ItemProductResponse> getData() {
        return data;
    }

    public void setData(ArrayList<ItemProductResponse> data) {
        this.data = data;
    }
}