package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.domain.interactor.TrackerRemoveInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackerRemoveDataRepository {
    void removeAgentData(TrackerRemoveInteractor.Callback aCallback);
}