package com.jmt.sg.sgcontrol.tracking.data_source;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Looper;

import com.jmt.sg.sgcontrol.tracking.GpsConstant;
import com.jmt.sg.sgcontrol.tracking.service.LocationFusedService;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;

/**
 * Created by jmtech on 5/24/16.
 */
public class GpsDataService implements GpsDataServiceI, Observable.OnSubscribe<Location> {
    private Intent IntentGpsService;
    private Context ctx;
    private Subscriber<? super Location> subscriberGpsService;

    public GpsDataService(Context context){
        this.ctx = context;
        IntentGpsService = new Intent(this.ctx,LocationFusedService.class);
    }

    @Override
    public void startGpsService() {
        ctx.startService(IntentGpsService);
    }

    @Override
    public void stopGpsService() {
        System.out.println("stopGpsService()");
        ctx.stopService(IntentGpsService);
    }

    @Override
    public Observable<Location> getLocationUpdates() {
        return Observable.create(this);
    }


    private void startLocationFusedReceiver() {
        System.out.println("..startGeoLocationReceiver()");
        ctx.registerReceiver(mGPSReceiver, new IntentFilter(GpsConstant.RECEIVE_GPS));
    }

    private void stopLocationFusedReceiver() {
        try {
            System.out.println("..stopGeoLocationReceiver");
            ctx.unregisterReceiver(mGPSReceiver);
        } catch (IllegalArgumentException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver mGPSReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println(GpsConstant.gpsLocation.getLatitude()+","+GpsConstant.gpsLocation.getLongitude());
            subscriberGpsService.onNext(GpsConstant.gpsLocation);
        }
    };

    @Override
    public void call(Subscriber<? super Location> subscriber) {
        subscriberGpsService = subscriber;
        startLocationFusedReceiver();
        subscriber.add(unsubscribeInUiThread(new Action0() {
            @Override public void call() {
                stopLocationFusedReceiver();
            }
        }));
    }

    private Subscription unsubscribeInUiThread(final Action0 unsubscribe) {
        return Subscriptions.create(new Action0() {
            @Override public void call() {
                if (Looper.getMainLooper() == Looper.myLooper()) {
                    unsubscribe.call();
                } else {
                    final Scheduler.Worker inner = AndroidSchedulers.mainThread().createWorker();
                    inner.schedule(new Action0() {
                        @Override public void call() {
                            unsubscribe.call();
                            inner.unsubscribe();
                        }
                    });
                }
            }
        });
    }
}