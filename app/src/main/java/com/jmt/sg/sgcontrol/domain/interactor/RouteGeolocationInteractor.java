package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.model.LocationHistoryE;
import com.jmt.sg.sgcontrol.domain.repository.RouteGeolocationRepository;
import com.jmt.sg.sgcontrol.tracking.helper.LocationHelper;

/**
 * Created by jmtech on 5/12/16.
 */
public class RouteGeolocationInteractor {
    private final RouteGeolocationRepository routeGeolocationRepository;

    public RouteGeolocationInteractor(RouteGeolocationRepository routeGeolocationRepository) {
        this.routeGeolocationRepository = routeGeolocationRepository;
    }

    public void storeGeolocation(double lat, double lng, final Callback placeCallback) {
        bestApproach(lat, lng, placeCallback);
    }

    private void bestApproach(double lat, double lng, final Callback placeCallback) {
        if(LocationHelper.PI == null){

            LocationHistoryE locationH = new LocationHistoryE(lat, lng);

            LocationHelper.PI = locationH;

            routeGeolocationRepository.storeGeolocation(lat, lng, placeCallback);
        }else{
            double latitude = LocationHelper.PI.getLat();
            double longitude = LocationHelper.PI.getLng();
            double distance = LocationHelper.calcDistance(lat, lng, latitude, longitude);

            LocationHistoryE locationH = new LocationHistoryE(lat, lng, distance);

            LocationHelper.locationHistoryE[LocationHelper.postLocationHistory++] = locationH;
        }


        if(LocationHelper.postLocationHistory == LocationHelper.maxLocationHistory){
            LocationHelper.PI = LocationHelper.adjustLocations(LocationHelper.locationHistoryE);
            System.out.println("GPS-D send PI-->"+LocationHelper.PI);

            routeGeolocationRepository.storeGeolocation(LocationHelper.PI.getLat(), LocationHelper.PI.getLng(), placeCallback);

            LocationHelper.locationHistoryE = new LocationHistoryE[LocationHelper.maxLocationHistory];
            LocationHelper.postLocationHistory = 0;

        }

    }


    public interface Callback {
        void onRegisterGeolocationSuccess();
        void onRegisterGeolocationError(String message);
    }
}