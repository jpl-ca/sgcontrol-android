package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.LogoutFragment;

public class LogoutActivity extends BaseActivity implements LogoutFragment.OnLogoutListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
    }

    @Override
    public void toLoginActivity() {
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }
}