package com.jmt.sg.sgcontrol.presentation.utils.ui;

import android.app.ActivityManager;
import android.content.Context;

/**
 * Created by jmtech on 5/23/16.
 */
public class Utils {
    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}