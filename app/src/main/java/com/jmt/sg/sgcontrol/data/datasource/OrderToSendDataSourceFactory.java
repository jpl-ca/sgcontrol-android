package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.data.datasource.db.DbOrderToSendDataSource;

/**
 * Created by jmtech on 5/13/16.
 */
public class OrderToSendDataSourceFactory {
    private final Context context;

    public OrderToSendDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public OrderToSendDataSource create(DataSourceFactory dataSourceFactory) {
        OrderToSendDataSource orderToSendDataSource = null;
        switch (dataSourceFactory) {
            case DB:
                orderToSendDataSource = new DbOrderToSendDataSource(context);
                break;
        }
        return orderToSendDataSource;
    }
}