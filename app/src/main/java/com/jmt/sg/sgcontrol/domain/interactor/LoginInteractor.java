package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.repository.LoginRepository;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public class LoginInteractor {
    private final LoginRepository loginRepository;

    public LoginInteractor(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    public void loginAgent(String identification_code, String password, final Callback placeCallback) {
        loginRepository.loginAgent(identification_code, password, placeCallback);
    }

    public interface Callback {
        void onLoginSuccess(AgentTrackeable agentTrackeable);
        void onLoginError(String message);
    }
}