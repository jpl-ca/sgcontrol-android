package com.jmt.sg.sgcontrol.presentation.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.data.datasource.SendFormDataSourceFactory;
import com.jmt.sg.sgcontrol.data.mapper.SendFormDataMapper;
import com.jmt.sg.sgcontrol.data.repository.SendFormDataRepository;
import com.jmt.sg.sgcontrol.domain.interactor.SendFormInteractor;
import com.jmt.sg.sgcontrol.domain.model.FormE;
import com.jmt.sg.sgcontrol.domain.model.FormToSendE;
import com.jmt.sg.sgcontrol.domain.model.FormValuesE;
import com.jmt.sg.sgcontrol.domain.repository.SendFormRepository;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.RegisterFormFragment;
import com.jmt.sg.sgcontrol.presentation.view.RegisterFormView;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public class RegisterFormPresenter implements Presenter<RegisterFormView>,SendFormInteractor.Callback {
    private Context context;
    private RegisterFormView registerFormView;
    private FormE formToRegister;
    private long route_visit_id;

    private SendFormInteractor sendFormInteractor;
    ArrayList<FormValuesE> form_fields;

    private FormToSendE formToSend;

    @Override
    public void addView(RegisterFormView view) {
        registerFormView = view;
        context = view.getContext();

        formToSend = new FormToSendE();

        SendFormRepository sendFormRepository = new SendFormDataRepository(new SendFormDataSourceFactory(context), new SendFormDataMapper(new Gson()));
        sendFormInteractor = new SendFormInteractor(sendFormRepository);

        formToRegister = (FormE) registerFormView.getAppActivity().getIntent().getSerializableExtra(RegisterFormFragment.ROUTE_FORM_DATA);
        route_visit_id = (long) registerFormView.getAppActivity().getIntent().getSerializableExtra(RegisterFormFragment.ROUTE_VISIT_ID);
        loadInitialForm();
    }

    private void loadInitialForm() {
        form_fields = formToRegister.getForm_values();
        registerFormView.showFormsList(form_fields);
    }

    @Override
    public void removeView() {
        registerFormView = null;
    }

    public void setFormInputData(int pos, String value) {
        form_fields.get(pos).setValue(value);
    }

    public void sendFormToRegister() {
        formToSend.setDescription("Enviando orden desde SGControl");

        if(formToRegister.getForm_template_id()>0)
            formToSend.setForm_template_id(formToRegister.getForm_template_id());
        else formToSend.setForm_template_id(formToRegister.getId());

        formToSend.setFormValues(form_fields);
        sendFormInteractor.sendForm(route_visit_id,formToSend,this);
    }

    @Override
    public void onSendFormSuccess() {
        registerFormView.registerFromSent(form_fields);
        registerFormView.showMessage(context.getString(R.string.s_formulario_actualizado));
    }

    @Override
    public void onSendFormError(String message) {
        registerFormView.showMessage(context.getString(R.string.s_error_al_actualizar));
    }
}