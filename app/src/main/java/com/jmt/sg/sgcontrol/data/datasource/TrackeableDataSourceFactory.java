package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.db.DbTrackeableDataSource;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class TrackeableDataSourceFactory {
    private final Context context;

    public TrackeableDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public TrackeableDataSource create(DataSourceFactory dataSource) {

        TrackeableDataSource trackeableDataSource = null;
        switch (dataSource) {
            case DB:
                trackeableDataSource = new DbTrackeableDataSource(context);
                break;
        }
        return trackeableDataSource;
    }
}