package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.TimelineFragment;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.VisitOrderFragment;
import com.jmt.sg.sgcontrol.presentation.utils.Constants;

import butterknife.ButterKnife;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class TimelineActivity extends AppCompatActivity implements TimelineFragment.OnTimeLineListener {

    private final TimelineFragment timelineFragment = TimelineFragment.instance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        ButterKnife.bind(this);
        showTimelineFragment();
    }

    private void showTimelineFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, timelineFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void visitDetailActivity(RouteVisitE routeVisit) {
        Intent it = new Intent(this,VisitOrderActivity.class);
        it.putExtra(VisitOrderFragment.ROUTE_VISIT_DATA,routeVisit);
        startActivityForResult(it,VisitOrderActivity.CODE_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VisitOrderActivity.CODE_RESULT) {
            if(resultCode == RESULT_OK){
                RouteVisitE routeVisit = (RouteVisitE) data.getSerializableExtra(Constants.VISIT_STATE.VisitState);
                timelineFragment.updateRouteVisit(routeVisit);
            }
            if (resultCode == RESULT_CANCELED) {
            }
        }
    }
}