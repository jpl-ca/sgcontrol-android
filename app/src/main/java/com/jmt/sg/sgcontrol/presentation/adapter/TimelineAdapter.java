package com.jmt.sg.sgcontrol.presentation.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.presentation.utils.Constants;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/20/16.
 */
public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.ViewHolder> {
    private Context ctx;
    private TimelineListener timelineListener;
    private ArrayList<RouteVisitE> data;
    public TimelineAdapter(Context _ctx, ArrayList<RouteVisitE> data, TimelineListener timelineListener){
        ctx = _ctx;
        this.data = data;
        this.timelineListener = timelineListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_timeline, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        RouteVisitE obj = data.get(position);
        holder.ll_visit_state.setBackgroundResource(getColorFromState(obj.getVisit_state_id()));
        holder.iv_estado.setImageDrawable(getIconFromState(obj.getVisit_state_id()));
        String hora = obj.getTime();
        holder.tv_estado_visita.setText(getNameFromState(obj.getVisit_state_id()));
        holder.tv_tiempo.setText(hora != null?hora:"00:00:00");
        holder.tv_nombre.setText(obj.getName());
        holder.tv_direccion.setText(obj.getAddress());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timelineListener.onItemVisitClick(position);
            }
        });
    }

    private int getColorFromState(int visitState) {
        switch (visitState){
            case Constants.VISIT_STATE.Scheduled: return R.color.visit_state_schedule;
            case Constants.VISIT_STATE.Done: return R.color.visit_state_done;
            case Constants.VISIT_STATE.ReScheduling: return R.color.visit_state_reschedule;
        }
        return Color.GRAY;
    }
    private String getNameFromState(int visitState) {
        switch (visitState){
            case Constants.VISIT_STATE.Scheduled: return ctx.getString(R.string.state_programado);
            case Constants.VISIT_STATE.Done: return ctx.getString(R.string.state_realizado);
            case Constants.VISIT_STATE.ReScheduling: return ctx.getString(R.string.state_reprogramado);
        }
        return null;
    }
    private Drawable getIconFromState(int visitState) {
        switch (visitState){
            case Constants.VISIT_STATE.Scheduled: return ctx.getResources().getDrawable(R.mipmap.ic_visit_schedule);
            case Constants.VISIT_STATE.Done: return ctx.getResources().getDrawable(R.mipmap.ic_visit_done);
            case Constants.VISIT_STATE.ReScheduling: return ctx.getResources().getDrawable(R.mipmap.ic_visit_reschedule);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_tiempo, tv_nombre, tv_direccion, tv_estado_visita;
        private ImageView iv_estado;
        private LinearLayout ll_visit_state;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_tiempo = (TextView)itemView.findViewById(R.id.tv_tiempo);
            tv_estado_visita = (TextView)itemView.findViewById(R.id.tv_estado_visita);
            iv_estado = (ImageView)itemView.findViewById(R.id.iv_estado);
            tv_nombre = (TextView)itemView.findViewById(R.id.tv_nombre);
            tv_direccion = (TextView)itemView.findViewById(R.id.tv_direccion);
            ll_visit_state = (LinearLayout)itemView.findViewById(R.id.ll_visit_state);
        }
    }

    public interface TimelineListener{
        void onItemVisitClick(int pos);
    }
}