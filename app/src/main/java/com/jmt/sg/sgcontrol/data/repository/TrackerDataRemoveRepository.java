package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.datasource.TrackeableDataSource;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.domain.interactor.TrackerRemoveInteractor;
import com.jmt.sg.sgcontrol.domain.repository.TrackerRemoveDataRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class TrackerDataRemoveRepository implements TrackerRemoveDataRepository {
    private final TrackeableDataSourceFactory trackeableDataSourceFactory;

    public TrackerDataRemoveRepository(TrackeableDataSourceFactory trackeableDataSourceFactory) {
        this.trackeableDataSourceFactory = trackeableDataSourceFactory;
    }

    @Override
    public void removeAgentData(final TrackerRemoveInteractor.Callback aCallback) {
        TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
        trackeableDataSource.removeAgent(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                aCallback.onTrackerDataRemoved();
            }

            @Override
            public void onError(Object object) {
                String mess = object.toString();
                aCallback.onTrackerDataRemovedError(mess);
            }
        });
    }
}