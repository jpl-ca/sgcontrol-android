package com.jmt.sg.sgcontrol.presentation.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgcontrol.domain.model.FormValuesE;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public interface VisitOrderView extends BaseView {
    Context getContext();
    AppCompatActivity getAppActivity();
    void settingData(RouteVisitE routeVisitE);
    void onRequestError(String message);
    void setVisitPointState(int state);
    void showMessage(String message);
    void visitPointRescheduled();
}