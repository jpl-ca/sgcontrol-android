package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.domain.interactor.OrderToSendInteractor;
import com.jmt.sg.sgcontrol.domain.model.FormToSendE;

/**
 * Created by jmtech on 5/12/16.
 */
public interface OrderToSendRepository {

    void saveOrderToSend(long visit_id, FormToSendE orderToSend, final OrderToSendInteractor.Callback oCallback);

    void getOrderToSend(long visit_id, final OrderToSendInteractor.Callback oCallback);

    void removeOrderToSend(long visit_id, final OrderToSendInteractor.Callback oCallback);
}