package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.rest.RestSendFormRegisterDataSource;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class SendFormDataSourceFactory {
    private final Context context;

    public SendFormDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public SendFormDataSource create(DataSourceFactory dataSourceFactory) {
        SendFormDataSource sendFormDataSource = null;
        switch (dataSourceFactory) {
            case CLOUD:
                sendFormDataSource = new RestSendFormRegisterDataSource(context);
                break;
        }
        return sendFormDataSource;
    }
}