package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.rest.RestRouteVisitDataSource;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class RouteVisitDataSourceFactory {
    public static final int CLOUD = 2;

    private final Context context;

    public RouteVisitDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public RouteVisitDataSource create(DataSourceFactory dataSourceFactory) {
        RouteVisitDataSource routeVisitDataSource = null;
        switch (dataSourceFactory) {
            case CLOUD:
                routeVisitDataSource = new RestRouteVisitDataSource(context);
                break;
        }
        return routeVisitDataSource;
    }
}