package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.datasource.RouteVisitDataSource;
import com.jmt.sg.sgcontrol.data.datasource.RouteVisitDataSourceFactory;
import com.jmt.sg.sgcontrol.data.mapper.RouteVisitDataMapper;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.data.model.RouteVisitResponse;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.domain.repository.RouteVisitRepository;
import com.jmt.sg.sgcontrol.data.model.RouteResponse;
import com.jmt.sg.sgcontrol.domain.interactor.RouteVisitInteractor;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/13/16.
 */
public class RouteVisitDataRepository implements RouteVisitRepository {
    private final RouteVisitDataSourceFactory routeVisitDataSourceFactory;
    private final RouteVisitDataMapper visitDataMapper;

    public RouteVisitDataRepository(RouteVisitDataSourceFactory routeVisitDataSourceFactory, RouteVisitDataMapper visitDataMapper) {
        this.routeVisitDataSourceFactory = routeVisitDataSourceFactory;
        this.visitDataMapper = visitDataMapper;
    }

    @Override
    public void getRouteVisit(final RouteVisitInteractor.Callback rouInteractorCallback) {
        RouteVisitDataSource routeVisitDataSource = routeVisitDataSourceFactory.create(DataSourceFactory.CLOUD);
        routeVisitDataSource.getRouteVisitData(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                RouteResponse routeR = (RouteResponse)object;
                ArrayList<RouteVisitResponse> routeVisitsR = routeR.getData();
//                System.out.println("A->"+routeVisitsR.get(0).getProduct_order().getProduct_order_items().size());
                ArrayList<RouteVisitE> routeVisits = visitDataMapper.transformRoute(routeVisitsR);
//                System.out.println("B->"+routeVisits.get(0).getProduct_order().getProduct_order_items().size());
                rouInteractorCallback.onGetRouteSuccess(routeVisits);
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                rouInteractorCallback.onGetRouteError(message);
            }
        });
    }
}