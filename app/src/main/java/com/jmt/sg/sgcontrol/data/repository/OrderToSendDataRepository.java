package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.domain.interactor.OrderToSendInteractor;
import com.jmt.sg.sgcontrol.data.datasource.OrderToSendDataSourceFactory;
import com.jmt.sg.sgcontrol.domain.model.FormToSendE;
import com.jmt.sg.sgcontrol.domain.repository.OrderToSendRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class OrderToSendDataRepository implements OrderToSendRepository {

    private static final String TAG = "LoginDataRepository";
    private final OrderToSendDataSourceFactory orderToSendDataSourceFactory;

    public OrderToSendDataRepository(OrderToSendDataSourceFactory orderToSendDataSourceFactory) {
        this.orderToSendDataSourceFactory = orderToSendDataSourceFactory;
    }

    @Override
    public void saveOrderToSend(long visit_id, FormToSendE orderToSend, OrderToSendInteractor.Callback oCallback) {

    }

    @Override
    public void getOrderToSend(long visit_id, OrderToSendInteractor.Callback oCallback) {

    }

    @Override
    public void removeOrderToSend(long visit_id, OrderToSendInteractor.Callback oCallback) {

    }
}