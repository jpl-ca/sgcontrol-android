package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.OrganizationFragment;

public class OrganizationActivity extends BaseActivity implements OrganizationFragment.OnOrganizationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);
    }

    @Override
    public void nextActivity() {
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }
}