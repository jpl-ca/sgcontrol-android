package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.preferences.PreferencesSGInfoDataSource;

/**
 * Created by jmtech on 5/13/16.
 */
public class SGInfoDataSourceFactory {
    public static final int PREFERENCES = 3;

    private final Context context;

    public SGInfoDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public SGInfoDataSource create(int dataSource) {

        SGInfoDataSource sgInfoDataSource = null;
        switch (dataSource) {
            case PREFERENCES:
                sgInfoDataSource = new PreferencesSGInfoDataSource(context);
                break;
        }
        return sgInfoDataSource;
    }
}