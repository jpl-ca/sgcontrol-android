package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.data.model.Trackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SGInfoRepository {
    void saveTrackableType(Trackeable trackeable);
    Trackeable getTrackableType();
}