package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.datasource.RescheduleVisitDataSource;
import com.jmt.sg.sgcontrol.data.datasource.RescheduleVisitDataSourceFactory;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.domain.interactor.RescheduleVisitInteractor;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.domain.repository.RescheduleVisitRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class RescheduleVisitDataRepository implements RescheduleVisitRepository {

    private final RescheduleVisitDataSourceFactory rescheduleVisitDataSourceFactory;

    public RescheduleVisitDataRepository(RescheduleVisitDataSourceFactory rescheduleVisitDataSourceFactory) {
        this.rescheduleVisitDataSourceFactory = rescheduleVisitDataSourceFactory;
    }

    @Override
    public void rescheduleVisit(long visit_point_id, String comment, final RescheduleVisitInteractor.Callback loginIteractorCallback) {
        RescheduleVisitDataSource rescheduleVisitDataSource = rescheduleVisitDataSourceFactory.create(DataSourceFactory.CLOUD);
        rescheduleVisitDataSource.rescheduleVisit(visit_point_id,comment,new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                loginIteractorCallback.onRescheduleSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                loginIteractorCallback.onRescheduleError(message);
            }
        });
    }
}