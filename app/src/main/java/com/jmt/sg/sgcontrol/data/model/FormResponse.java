package com.jmt.sg.sgcontrol.data.model;

import com.jmt.sg.sgcontrol.data.mapper.BaseResponse;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class FormResponse extends BaseResponse {
    private long id;

    private String title;

    private String description;

    private String state;

    private long form_template_id;

    private ArrayList<FormValuesResponse> form_values;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getForm_template_id() {
        return form_template_id;
    }

    public void setForm_template_id(long form_template_id) {
        this.form_template_id = form_template_id;
    }

    public ArrayList<FormValuesResponse> getForm_values() {
        return form_values;
    }

    public void setForm_values(ArrayList<FormValuesResponse> form_values) {
        this.form_values = form_values;
    }
}