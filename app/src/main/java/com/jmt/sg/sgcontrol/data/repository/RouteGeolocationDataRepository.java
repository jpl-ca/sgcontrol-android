package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.datasource.RouteGeolocationDataSource;
import com.jmt.sg.sgcontrol.data.datasource.RouteGeolocationDataSourceFactory;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.domain.interactor.RouteGeolocationInteractor;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.domain.repository.RouteGeolocationRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class RouteGeolocationDataRepository implements RouteGeolocationRepository {

    private static final String TAG = "LoginDataRepository";
    private final RouteGeolocationDataSourceFactory routeGeolocationDataSourceFactory;

    public RouteGeolocationDataRepository(RouteGeolocationDataSourceFactory routeGeolocationDataSourceFactory) {
        this.routeGeolocationDataSourceFactory = routeGeolocationDataSourceFactory;
    }

    @Override
    public void storeGeolocation(double lat, double lng, final RouteGeolocationInteractor.Callback rCallback) {
        RouteGeolocationDataSource geolocationDataSource = routeGeolocationDataSourceFactory.create(DataSourceFactory.CLOUD);
        geolocationDataSource.storeGeolocation(lat, lng, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                rCallback.onRegisterGeolocationSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                System.out.println(TAG+"->"+message);
                rCallback.onRegisterGeolocationError(message);
            }
        });
    }
}