package com.jmt.sg.sgcontrol.data.datasource.db;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.TrackeableDataSource;
import com.jmt.sg.sgcontrol.data.datasource.db.realm.AgentDataService;
import com.jmt.sg.sgcontrol.data.datasource.db.realm.AgentDataServiceI;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by jmtech on 5/13/16.
 */
public class DbTrackeableDataSource implements TrackeableDataSource {

    private CompositeSubscription compositeSubscription;
    private AgentDataServiceI agentDataService;
    private final Context context;

    public DbTrackeableDataSource(Context context) {
        this.context = context;
        agentDataService = new AgentDataService(context);
        compositeSubscription = new CompositeSubscription();
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    @Override
    public void saveAgent(AgentTrackeable agentTrackeable, final RepositoryCallback repositoryCallback) {
        Subscription subscription = agentDataService.registerAgent(agentTrackeable).
            subscribeOn(Schedulers.io()).
            observeOn(AndroidSchedulers.mainThread()).
            subscribe(
                    new Action1<Boolean>() {
                        @Override
                        public void call(Boolean success) {
                            System.out.println("Agent fue insertado...>"+success);
                        }
                    },
                    new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
            );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    @Override
    public void getAgent(final RepositoryCallback repositoryCallback) {
        Subscription subscription = agentDataService.getAgent().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<AgentTrackeable>() {
                            @Override
                            public void call(AgentTrackeable track) {
                                repositoryCallback.onSuccess(track);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    @Override
    public void removeAgent(final RepositoryCallback repositoryCallback) {
        Subscription subscription = agentDataService.removeAgent().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<Boolean>() {
                            @Override
                            public void call(Boolean track) {
                                repositoryCallback.onSuccess(track);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

}