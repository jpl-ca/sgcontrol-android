package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.presenter.SessionPresenter;
import com.jmt.sg.sgcontrol.presentation.view.SessionView;

import butterknife.Bind;

/**
 * A placeholder fragment containing a simple view.
 */
public class SplashFragment extends BaseFragment implements SessionView {
    @Bind(R.id.rl_root) RelativeLayout ll_root;

    private OnSplashListener presenter;
    private SessionPresenter sessionPresenter;

    public static SplashFragment instance(){
        return new SplashFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash, container, false);
        injectView(rootView);

        sessionPresenter = new SessionPresenter();
        sessionPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        sessionPresenter.getSession();
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnSplashListener) {
            presenter = (OnSplashListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        sessionPresenter.removeView();
    }

    @Override
    public void showErrorMessage(String message) {
        showMessage(ll_root,message);
    }

    @Override
    public void checking() {

    }

    @Override
    public void hasSession(boolean b) {
        if(b) {
            presenter.toHomeActivity();
        } else {
            presenter.toLoginActivity();
        }
    }

    public interface OnSplashListener{
        void toLoginActivity();
        void toHomeActivity();
    }
}