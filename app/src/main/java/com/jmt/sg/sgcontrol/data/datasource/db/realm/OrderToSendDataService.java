package com.jmt.sg.sgcontrol.data.datasource.db.realm;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.db.entity.OrderToSendRealm;
import com.jmt.sg.sgcontrol.data.datasource.db.rx.RealmObservable;
import com.jmt.sg.sgcontrol.data.model.FormToSendEntity;

import io.realm.Realm;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by jmtech on 5/16/16.
 */
public class OrderToSendDataService implements OrderToSendDataServiceI{
    private final Context context;

    public OrderToSendDataService(Context context){
        this.context = context;
    }

    @Override
    public Observable<FormToSendEntity> getOrderToSend(long visit_id) {
        return RealmObservable.object(context, new Func1<Realm, OrderToSendRealm>() {
            @Override
            public OrderToSendRealm call(Realm realm) {
                OrderToSendRealm agentR = realm.where(OrderToSendRealm.class).findFirst();
                return agentR;
            }
        }).map(new Func1<OrderToSendRealm, FormToSendEntity>() {
            @Override
            public FormToSendEntity call(OrderToSendRealm realmUser) {
                FormToSendEntity order = new FormToSendEntity();
//                TODO Complete Here
                return order;
            }
        });
    }

    @Override
    public Observable<Boolean> registerOrderToSend(long visit_id, FormToSendEntity user) {
        return RealmObservable.object(context, new Func1<Realm, OrderToSendRealm>() {
            @Override
            public OrderToSendRealm call(Realm realm) {
                OrderToSendRealm agentR = new OrderToSendRealm();
//                TODO Complete Here
                return agentR;
            }
        }).map(new Func1<OrderToSendRealm, Boolean>() {
            @Override
            public Boolean call(OrderToSendRealm agentR) {
                return agentR != null;
            }
        });
    }

    @Override
    public Observable<Boolean> removeOrderToSend(long visit_id) {
        return RealmObservable.object(context, new Func1<Realm, OrderToSendRealm>() {
            @Override
            public OrderToSendRealm call(Realm realm) {
                OrderToSendRealm us = realm.where(OrderToSendRealm.class).findFirst();
                realm.where(OrderToSendRealm.class).findAll().clear();
                return us;
            }
        }).map(new Func1<OrderToSendRealm, Boolean>() {
            @Override
            public Boolean call(OrderToSendRealm realmUser) {
                return true;
            }
        });
    }
}
