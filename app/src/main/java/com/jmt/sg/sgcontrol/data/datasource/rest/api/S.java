package com.jmt.sg.sgcontrol.data.datasource.rest.api;

/**
 * Created by jmtech on 5/13/16.
 */
public final class S {

    public static final class VAR{
        public static final String SERVER_URL = "http://159.203.254.124:8080";
        public static final String PREF_TOKEN = "Bearer";
        public static final String TOKEN = "Token";
        public static final String COOKIE = "Cookie";
        public static final String SET_COOKIE = "Set-Cookie";
        public static final String Accept = "application/json";
    }
}