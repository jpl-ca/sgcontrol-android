package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.presentation.adapter.TimelineAdapter;
import com.jmt.sg.sgcontrol.presentation.presenter.TrackeablePresenter;
import com.jmt.sg.sgcontrol.presentation.view.TrackeableView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class TimelineFragment extends BaseFragment implements TimelineAdapter.TimelineListener,TrackeableView {
    @BindString(R.string.t_agenda) protected String titlebar;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.ll_root) RelativeLayout ll_root;
    @Bind(R.id.rv_timeline) protected RecyclerView rv_timeline;
    @Bind(R.id.empty_view) protected TextView empty_view;

    LayoutInflater inflater;
    private Context context;

    public static String ROUTE_VISITS_DATA = "RouteVisitsData";
    private ArrayList<RouteVisitE> routeVisits;

    private OnTimeLineListener presenter;
    private TrackeablePresenter trackeablePresenter;

    public static TimelineFragment instance(){
        return new TimelineFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_timeline, container, false);
        ButterKnife.bind(this, rootView);
        this.inflater = inflater;
        this.context = getContext();

        setToolbar(toolbar, titlebar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        trackeablePresenter = new TrackeablePresenter();
        trackeablePresenter.addView(this);

        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnTimeLineListener) {
            presenter = (OnTimeLineListener) context;
        } else {
            throw new ClassCastException("debe implementar On"+getClass().getSimpleName()+"Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        routeVisits = (ArrayList<RouteVisitE>) activity.getIntent().getSerializableExtra(ROUTE_VISITS_DATA);
        if(routeVisits == null)routeVisits = new ArrayList<>();

        trackeablePresenter.getTrackeableInfo();

        if(routeVisits.size()>0){
            showTimeline();
            empty_view.setVisibility(View.GONE);
        }else{
            empty_view.setVisibility(View.VISIBLE);
            rv_timeline.setVisibility(View.GONE);
        }
    }

    private void showTimeline() {
        TimelineAdapter oAdapter = new TimelineAdapter(getContext(), routeVisits, this);

        rv_timeline.setAdapter(oAdapter);
        rv_timeline.setHasFixedSize(true);
        rv_timeline.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        trackeablePresenter.removeView();
    }

    @Override
    public void showData(AgentTrackeable agentTrackeable) {
        setToolbar(toolbar, agentTrackeable.getFirst_name() + " " + agentTrackeable.getLast_name());
    }

    @Override
    public void showErrorMessage(String message) {
        showMessage(ll_root, message);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:presenter.onBackPressed();break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onItemVisitClick(int pos) {
        presenter.visitDetailActivity(routeVisits.get(pos));
    }

    public void updateRouteVisit(RouteVisitE routeVisit) {
        for (int i=0; i<routeVisits.size(); i++) {
            if(routeVisits.get(i).getId()==routeVisit.getId()){
                routeVisits.set(i,routeVisit);
                break;
            }
        }
        showTimeline();
    }

    public interface OnTimeLineListener{
        void visitDetailActivity(RouteVisitE routeVisit);
        void onBackPressed();
    }
}