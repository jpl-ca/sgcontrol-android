package com.jmt.sg.sgcontrol.data.datasource.rest.service;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.rest.api.ApiService;
import com.jmt.sg.sgcontrol.data.model.FormToSendEntity;
import com.jmt.sg.sgcontrol.data.model.RouteResponse;

import org.json.JSONObject;

import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class RouteVisitService extends ApiService{
    private RouteVisitApi apiService;

    public RouteVisitService(Context context) {
        super(context);
        apiService = retrofit.create(RouteVisitApi.class);
    }

    public RouteVisitApi getApi() {
        return apiService;
    }

    public interface RouteVisitApi{
        @GET(_api+"/auth/trackable/me/forms/tasks-visit-points/today")
        Observable<RouteResponse> getCurrentRoute();

        @POST(_api+"/auth/trackable/me/forms/tasks-visit-points/{visit_point_id}/form")
        Observable<JSONObject> sendFormRegister(@Path("visit_point_id") long visit_point_id, @Body FormToSendEntity body);

        @FormUrlEncoded
        @POST(_api+"/auth/me/reschedule-visit-point")
        Observable<JSONObject> rescheduleVisitPoints(@Field("id") long visit_point_id,@Field("comment_of_rescheduling") String comment_of_rescheduling);
    }
}