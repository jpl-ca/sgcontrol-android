package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.domain.repository.TrackerSaveDataRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class TrackerSaveInteractor {
    private final TrackerSaveDataRepository trackerRepository;

    public TrackerSaveInteractor(TrackerSaveDataRepository trackerRepository) {
        this.trackerRepository = trackerRepository;
    }

    public void saveTrackerData(AgentTrackeable aTrackeable, final Callback aCallback) {
        trackerRepository.saveTrackerData(aTrackeable, aCallback);
    }

    public interface Callback {
        void onTrackerDataSaved();
        void onTrackerDataSavedError(String mess);
    }

}