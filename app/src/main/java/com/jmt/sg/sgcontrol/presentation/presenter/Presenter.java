package com.jmt.sg.sgcontrol.presentation.presenter;

/**
 * Created by jmtech on 5/12/16.
 */
public interface Presenter<T> {
    void addView(T view);
    void removeView();
}