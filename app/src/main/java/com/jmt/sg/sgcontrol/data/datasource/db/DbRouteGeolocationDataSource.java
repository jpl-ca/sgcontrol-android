package com.jmt.sg.sgcontrol.data.datasource.db;

import android.content.Context;

import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.data.datasource.RouteGeolocationDataSource;

/**
 * Created by jmtech on 5/13/16.
 */
public class DbRouteGeolocationDataSource implements RouteGeolocationDataSource {
    private final Context context;

    public DbRouteGeolocationDataSource(Context context) {
        this.context= context;
    }

    @Override
    public void storeGeolocation(double lat, double lng, RepositoryCallback repositoryCallback) {

    }
}