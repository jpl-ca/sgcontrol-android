package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.LoginFragment;

public class LoginActivity extends BaseActivity implements LoginFragment.OnLoginListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void nextActivity() {
        startActivity(new Intent(this,MapHomeActivity.class));
        finish();
    }

    @Override
    public void validateOrganizationActivity() {
        startActivity(new Intent(this,OrganizationActivity.class));
        finish();
    }
}