package com.jmt.sg.sgcontrol.data.model;

import com.jmt.sg.sgcontrol.data.mapper.BaseResponse;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class RouteResponse extends BaseResponse {
    private ArrayList<RouteVisitResponse> data;

    private String message;

    public ArrayList<RouteVisitResponse> getData() {
        return data;
    }

    public void setData(ArrayList<RouteVisitResponse> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}