package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.presenter.LogoutPresenter;
import com.jmt.sg.sgcontrol.presentation.view.LogoutView;

import butterknife.Bind;

/**
 * A placeholder fragment containing a simple view.
 */
public class LogoutFragment extends BaseFragment implements LogoutView {
    @Bind(R.id.rl_root) RelativeLayout ll_root;

    private OnLogoutListener presenter;
    private LogoutPresenter logoutPresenter;

    public static LogoutFragment instance(){
        return new LogoutFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_logout, container, false);
        injectView(rootView);

        logoutPresenter = new LogoutPresenter();
        logoutPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        logoutPresenter.closeSession();
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnLogoutListener) {
            presenter = (OnLogoutListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        logoutPresenter.removeView();
    }

    @Override
    public void onLogout() {
        presenter.toLoginActivity();
    }

    @Override
    public void showErrorMessage(String message) {
        showMessage(ll_root,message);
    }

    public interface OnLogoutListener{
        void toLoginActivity();
    }
}