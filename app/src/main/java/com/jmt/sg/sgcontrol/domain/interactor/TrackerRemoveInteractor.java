package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.repository.TrackerRemoveDataRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class TrackerRemoveInteractor {
    private final TrackerRemoveDataRepository trackerRepository;

    public TrackerRemoveInteractor(TrackerRemoveDataRepository trackerRepository) {
        this.trackerRepository = trackerRepository;
    }

    public void removeTrackerAgentData(final Callback vCallback) {
        trackerRepository.removeAgentData(vCallback);
    }

    public interface Callback {
        void onTrackerDataRemoved();
        void onTrackerDataRemovedError(String mess);
    }

}