package com.jmt.sg.sgcontrol.data.datasource;

import com.jmt.sg.sgcontrol.data.model.Trackeable;

/**
 * Created by jmtech on 5/13/16.
 */
public interface SGInfoDataSource {
    void saveTrackeableType(Trackeable trackeable);
    Trackeable getTrackeableType();
}