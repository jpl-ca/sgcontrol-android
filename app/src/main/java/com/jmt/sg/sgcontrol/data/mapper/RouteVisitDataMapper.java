package com.jmt.sg.sgcontrol.data.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jmt.sg.sgcontrol.data.model.RouteVisitResponse;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class RouteVisitDataMapper {
    Gson gson;

    public RouteVisitDataMapper(Gson gson){
        this.gson = gson;
    }

    public ArrayList<RouteVisitE> transformRoute(ArrayList<RouteVisitResponse> routeResponse){
        String routeStr = gson.toJson(routeResponse);
        System.out.println("A transformar:");
        System.out.println(routeStr);
        ArrayList<RouteVisitE> routeVisitList = gson.fromJson(routeStr, new TypeToken<ArrayList<RouteVisitE>>(){}.getType());
        System.out.println("Transformado:");
        System.out.println(gson.toJson(routeVisitList));
        return routeVisitList;
    }
}