package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.presentation.presenter.TrackeablePresenter;
import com.jmt.sg.sgcontrol.presentation.view.TrackeableView;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class ProfileFragment extends BaseFragment implements TrackeableView {
    @BindString(R.string.t_inicio) protected String titlebar;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.btn_cerrar_sesion) Button btn_cerrar_sesion;
    @Bind(R.id.ll_profile_data) LinearLayout ll_profile_data;
    private OnProfileListener presenter;

    private LayoutInflater inflater;
    private TrackeablePresenter trackeablePresenter;

    public static ProfileFragment instance(){
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);

        setToolbar(toolbar, titlebar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        this.inflater = inflater;
        trackeablePresenter = new TrackeablePresenter();
        trackeablePresenter.addView(this);
        trackeablePresenter.getTrackeableInfo();

        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnProfileListener) {
            presenter = (OnProfileListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:presenter.onBackPressed();break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_cerrar_sesion)
    public void logout(){
        presenter.logoutActivity();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void showError(String s) {
        Snackbar.make(ll_root, s, Snackbar.LENGTH_LONG).show();
    }

    public void addView(String key, String value, Drawable ic){
        View vv  = inflater.inflate(R.layout.view_profile_data, ll_profile_data, false);
        final ImageView iv_icon = ((ImageView)vv.findViewById(R.id.iv_icon));
        final TextView txt_key = ((TextView)vv.findViewById(R.id.txt_key));
        final TextView txt_value = ((TextView)vv.findViewById(R.id.txt_value));
        iv_icon.setImageDrawable(ic);
        txt_key.setText(key);
        txt_value.setText(value);
        ll_profile_data.addView(vv);
    }

    @Override
    public void showData(AgentTrackeable agentTrackeable) {
        setToolbar(toolbar, getString(R.string.s_perfil_agente));
        addView(getString(R.string.p_organization),agentTrackeable.getOrganization_id(),getResources().getDrawable(R.drawable.ic_organization));
        addView(getString(R.string.p_nombre), agentTrackeable.getFirst_name(),getResources().getDrawable(R.drawable.ic_agent_firstname));
        addView(getString(R.string.p_apellido), agentTrackeable.getLast_name(),getResources().getDrawable(R.drawable.ic_agent_lastname));
        addView(getString(R.string.p_codigo_agente), agentTrackeable.getAuthentication_code(),getResources().getDrawable(R.drawable.ic_agent_code));

        btn_cerrar_sesion.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorMessage(String message) {
        showError(message);
    }

    public interface OnProfileListener{
        void onBackPressed();
        void logoutActivity();
    }
}