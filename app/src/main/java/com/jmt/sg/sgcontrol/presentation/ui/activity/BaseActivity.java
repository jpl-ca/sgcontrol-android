package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by jmtech on 5/12/16.
 */
public class BaseActivity extends AppCompatActivity{

    protected void injectView() {
        ButterKnife.bind(this);
    }

}