package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.rest.RestLoginDataSource;
import com.jmt.sg.sgcontrol.data.datasource.db.DbLoginDataSource;
import com.jmt.sg.sgcontrol.data.datasource.preferences.PreferencesLoginDataSource;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class LoginDataSourceFactory {
    private final Context context;

    public LoginDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public LoginDataSource create(DataSourceFactory dataSource) {

        LoginDataSource loginDataSource = null;
        switch (dataSource) {
            case CLOUD:
                loginDataSource = new RestLoginDataSource(context);
                break;
            case DB:
                loginDataSource = new DbLoginDataSource(context);
                break;
            case PREFERENCES:
                loginDataSource = new PreferencesLoginDataSource(context);
                break;
        }
        return loginDataSource;
    }
}