package com.jmt.sg.sgcontrol.presentation.view;

import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface ProfileView extends BaseView {
    void showAgentInfo(AgentTrackeable agentTrackeable);
    void showErrorMessage(String message);
}