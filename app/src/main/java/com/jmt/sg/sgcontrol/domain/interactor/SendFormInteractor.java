package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.repository.SendFormRepository;
import com.jmt.sg.sgcontrol.domain.model.FormToSendE;

/**
 * Created by jmtech on 5/12/16.
 */
public class SendFormInteractor {
    private final SendFormRepository sendFormRepository;

    public SendFormInteractor(SendFormRepository sendFormRepository) {
        this.sendFormRepository = sendFormRepository;
    }

    public void sendForm(long visitPointId, FormToSendE data, final Callback callback) {
        sendFormRepository.sendForm(visitPointId, data, callback);
    }

    public interface Callback {
        void onSendFormSuccess();
        void onSendFormError(String message);
    }
}