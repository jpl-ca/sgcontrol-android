package com.jmt.sg.sgcontrol.data.datasource;

import com.jmt.sg.sgcontrol.data.model.FormToSendEntity;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface OrderToSendDataSource {
    void saveOrderToSend(long visit_id, FormToSendEntity user, RepositoryCallback repositoryCallback);

    void getOrderToSend(long visit_id, RepositoryCallback repositoryCallback);

    void removeOrderToSend(long visit_id, RepositoryCallback repositoryCallback);

}