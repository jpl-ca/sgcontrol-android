package com.jmt.sg.sgcontrol.presentation.utils.media;

import android.widget.ImageView;

import com.jmt.sg.sgcontrol.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by jmtech on 5/12/16.
 */
public class PicassoILoader implements ImageLoader {

    @Override
    public void load(String url, ImageView imageView) {
        Picasso.with(imageView.getContext()).load(url).fit().placeholder(R.mipmap.ic_bg_loading).memoryPolicy(MemoryPolicy.NO_CACHE).into(imageView);
    }

    @Override
    public void loadLocal(String path, ImageView imageView) {
        Picasso.with(imageView.getContext()).load(path).fit().placeholder(R.mipmap.ic_bg_loading).memoryPolicy(MemoryPolicy.NO_CACHE).into(imageView);
    }
}