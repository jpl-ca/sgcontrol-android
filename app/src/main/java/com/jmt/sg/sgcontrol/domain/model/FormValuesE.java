package com.jmt.sg.sgcontrol.domain.model;

import java.io.Serializable;

import io.realm.annotations.PrimaryKey;

/**
 * Created by jmtech on 5/16/16.
 */
public class FormValuesE implements Serializable {
    private long id;

    private long form_template_id;

    private String machine_name;

    private String label;

    private String type;

    private String description;

    private String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getForm_template_id() {
        return form_template_id;
    }

    public void setForm_template_id(long form_template_id) {
        this.form_template_id = form_template_id;
    }

    public String getMachine_name() {
        return machine_name;
    }

    public void setMachine_name(String machine_name) {
        this.machine_name = machine_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}