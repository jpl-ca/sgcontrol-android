package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.datasource.SessionDataSource;
import com.jmt.sg.sgcontrol.data.datasource.SessionDataSourceFactory;
import com.jmt.sg.sgcontrol.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.domain.interactor.SessionInteractor;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.jmt.sg.sgcontrol.domain.repository.SessionRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class SessionDataRepository implements SessionRepository {
    private final SessionDataSourceFactory sessionDataSourceFactory;
    private final TrackeableDataMapper trackeableDataMapper;

    public SessionDataRepository(SessionDataSourceFactory sessionDataSourceFactory, TrackeableDataMapper trackeableDataMapper) {
        this.sessionDataSourceFactory = sessionDataSourceFactory;
        this.trackeableDataMapper= trackeableDataMapper;
    }

    @Override
    public void checkSesion(final SessionInteractor.CheckSessionCallback sessionInteractorCallback) {
        SessionDataSource sessionDataSource = sessionDataSourceFactory.create(DataSourceFactory.CLOUD);
        sessionDataSource.checkSession(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                sessionInteractorCallback.onValidateSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                sessionInteractorCallback.onValidateError(message);
            }
        });
    }

    @Override
    public void closeSesion(final SessionInteractor.CloseSessionCallback logoutCallback) {
        SessionDataSource sessionDataSource = sessionDataSourceFactory.create(DataSourceFactory.CLOUD);
        sessionDataSource.closeSession(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                logoutCallback.onLogoutSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                logoutCallback.onLogoutError(message);
            }
        });
    }
}