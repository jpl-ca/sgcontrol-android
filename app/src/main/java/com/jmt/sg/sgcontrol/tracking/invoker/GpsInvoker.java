package com.jmt.sg.sgcontrol.tracking.invoker;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.jmt.sg.sgcontrol.tracking.data_source.GpsDataService;
import com.jmt.sg.sgcontrol.tracking.data_source.GpsDataServiceI;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by jmtech on 5/23/16.
 */
public class GpsInvoker implements Action1<Location>{
    private LocationCallback locationCallback;
    private final Context context;
    private final GpsDataServiceI gpsDataServiceI;

    private Subscription subscription;
    private Observable<Location> integerObservable;

    public GpsInvoker(Context context){
        this.context = context;
        gpsDataServiceI = new GpsDataService(context);
    }

    public void startGpsData(){
        gpsDataServiceI.startGpsService();
        System.out.println(".....startGpsData()");
    }

    public void startGpsListener(LocationCallback locationCallback){
        this.locationCallback = locationCallback;

        integerObservable = gpsDataServiceI.getLocationUpdates();
        subscription = integerObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this);
    }

    public void stopGpsListener(){
        locationCallback = null;
        subscription.unsubscribe();
    }

    public void stopGpsData(){
        System.out.println(".....stopGpsData()");
        gpsDataServiceI.stopGpsService();
    }

    @Override
    public void call(Location location) {
        if(locationCallback!=null)locationCallback.updateLocation(location);
        Log.d("PLAYGROUND", "call: " + location.getLatitude()+ "," + location.getLongitude());
    }

    public interface LocationCallback{
        void updateLocation(Location location);
    }
}