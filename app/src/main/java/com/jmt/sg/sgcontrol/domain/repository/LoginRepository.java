package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.domain.interactor.LoginInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface LoginRepository {
    void loginAgent(String identification_code, String password, final LoginInteractor.Callback loginIteractorCallback);
}