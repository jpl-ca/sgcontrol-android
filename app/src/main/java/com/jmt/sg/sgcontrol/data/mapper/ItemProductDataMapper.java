package com.jmt.sg.sgcontrol.data.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jmt.sg.sgcontrol.data.model.ItemProductResponse;
import com.jmt.sg.sgcontrol.domain.model.FormValuesE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class ItemProductDataMapper {
    Gson gson;

    public ItemProductDataMapper(Gson gson){
        this.gson = gson;
    }

    public ArrayList<FormValuesE> transformItemProduct(ArrayList<ItemProductResponse> objResponse){
        String str = gson.toJson(objResponse);
        ArrayList<FormValuesE> objTransform = gson.fromJson(str, new TypeToken<ArrayList<FormValuesE>>(){}.getType());
        return objTransform;
    }
}