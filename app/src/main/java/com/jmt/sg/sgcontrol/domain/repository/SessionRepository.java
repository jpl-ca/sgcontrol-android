package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.domain.interactor.SessionInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SessionRepository {
    void checkSesion(final SessionInteractor.CheckSessionCallback sessionInteractorCallback);
    void closeSesion(final SessionInteractor.CloseSessionCallback logoutCallback);
}