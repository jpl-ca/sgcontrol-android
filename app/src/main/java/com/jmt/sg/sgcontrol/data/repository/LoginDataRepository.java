package com.jmt.sg.sgcontrol.data.repository;

import com.jmt.sg.sgcontrol.data.datasource.LoginDataSource;
import com.jmt.sg.sgcontrol.data.datasource.LoginDataSourceFactory;
import com.jmt.sg.sgcontrol.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;
import com.jmt.sg.sgcontrol.data.model.LoginTrackeableResponse;
import com.jmt.sg.sgcontrol.domain.interactor.LoginInteractor;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.domain.repository.LoginRepository;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public class LoginDataRepository implements LoginRepository {

    private static final String TAG = "LoginDataRepository";
    private final LoginDataSourceFactory loginDataSourceFactory;
    private final TrackeableDataMapper trackeableDataMapper;

    public LoginDataRepository(LoginDataSourceFactory loginDataSourceFactory, TrackeableDataMapper trackeableDataMapper) {
        this.loginDataSourceFactory = loginDataSourceFactory;
        this.trackeableDataMapper= trackeableDataMapper;
    }

    @Override
    public void loginAgent(String identification_code, String password, final LoginInteractor.Callback loginIteractorCallback) {
        LoginDataSource validateOrganizationDataStore = loginDataSourceFactory.create(DataSourceFactory.CLOUD);
        validateOrganizationDataStore.loginAgent(identification_code, password, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                AgentTrackeable agentTrackeable = trackeableDataMapper.transformAgentResponse((LoginTrackeableResponse) object);
                loginIteractorCallback.onLoginSuccess(agentTrackeable);
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                System.out.println(TAG+"->"+message);
                loginIteractorCallback.onLoginError(message);
            }
        });
    }
}