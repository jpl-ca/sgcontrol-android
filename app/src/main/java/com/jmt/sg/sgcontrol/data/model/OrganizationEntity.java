package com.jmt.sg.sgcontrol.data.model;

import com.jmt.sg.sgcontrol.data.mapper.BaseResponse;

/**
 * Created by jmtech on 5/18/16.
 */
public class OrganizationEntity extends BaseResponse {
    private String id;
    private String name;
    private String ruc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
}