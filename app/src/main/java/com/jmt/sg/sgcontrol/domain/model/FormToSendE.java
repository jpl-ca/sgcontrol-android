package com.jmt.sg.sgcontrol.domain.model;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.annotations.PrimaryKey;

/**
 * Created by jmtech on 5/16/16.
 */
public class FormToSendE implements Serializable{

    private long id;

    private String description;

    private long tasks_visit_point_id;

    private long form_template_id;

    private String status;

    private ArrayList<FormValuesE> formValues;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTasks_visit_point_id() {
        return tasks_visit_point_id;
    }

    public void setTasks_visit_point_id(long tasks_visit_point_id) {
        this.tasks_visit_point_id = tasks_visit_point_id;
    }

    public long getForm_template_id() {
        return form_template_id;
    }

    public void setForm_template_id(long form_template_id) {
        this.form_template_id = form_template_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<FormValuesE> getFormValues() {
        return formValues;
    }

    public void setFormValues(ArrayList<FormValuesE> formValues) {
        this.formValues = formValues;
    }
}