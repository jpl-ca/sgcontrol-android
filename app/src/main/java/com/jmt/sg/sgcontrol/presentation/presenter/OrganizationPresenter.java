package com.jmt.sg.sgcontrol.presentation.presenter;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.repository.ValidateOrganizationDataRepository;
import com.jmt.sg.sgcontrol.presentation.view.OrganizationView;
import com.jmt.sg.sgcontrol.data.datasource.ValidateOrganizationDataSourceFactory;
import com.jmt.sg.sgcontrol.domain.interactor.OrganizationInteractor;
import com.jmt.sg.sgcontrol.domain.repository.OrganizationRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class OrganizationPresenter implements Presenter<OrganizationView>,OrganizationInteractor.Callback {

    private Context context;
    private OrganizationView organizationView;
    private OrganizationInteractor organizationInteractor;

    @Override
    public void addView(OrganizationView view) {
        organizationView = view;
        context = view.getContext();

        OrganizationRepository organizationRepository = new ValidateOrganizationDataRepository(new ValidateOrganizationDataSourceFactory(context));
        organizationInteractor = new OrganizationInteractor(organizationRepository);

    }

    @Override
    public void removeView() {
        organizationView = null;
    }

    public void validateOrganization(String organization_name){
        organizationView.showLoading();
        organizationInteractor.validateOrganization(organization_name, this);
    }

    @Override
    public void onValidateSuccess() {
        organizationView.successValidate();
    }

    @Override
    public void onValidateError(String message) {
        organizationView.hideLoading();
        organizationView.showErrorMessage(message);
    }
}