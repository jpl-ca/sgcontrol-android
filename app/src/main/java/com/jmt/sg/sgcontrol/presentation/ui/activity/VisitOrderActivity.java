package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.FormE;
import com.jmt.sg.sgcontrol.domain.model.FormValuesE;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.RegisterFormFragment;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.VisitOrderFragment;
import com.jmt.sg.sgcontrol.presentation.utils.Constants;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class VisitOrderActivity extends AppCompatActivity implements VisitOrderFragment.OnVisitOrderListener {

    public static final int CODE_RESULT = 101;

    private final VisitOrderFragment visitOrderFragment = VisitOrderFragment.instance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_order);
        showVisitOrderFragment();
    }

    private void showVisitOrderFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, visitOrderFragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RegisterFormActivity.CODE_RESULT) {
            if(resultCode == RESULT_OK){
                ArrayList<FormValuesE> form_refresh = (ArrayList<FormValuesE>) data.getSerializableExtra(RegisterFormFragment.ROUTE_FORM_DATA);
                visitOrderFragment.refreshRouteForm(form_refresh);
            }
            if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    @Override
    public void sendResultSuccess(RouteVisitE routeVisit) {
        Intent it = new Intent();
        it.putExtra(Constants.VISIT_STATE.VisitState,routeVisit);
        setResult(RESULT_OK,it);
    }

    @Override
    public void loadFormActivity(long visit_id,FormE form) {
        Intent it = new Intent(this, RegisterFormActivity.class);
        it.putExtra(RegisterFormFragment.ROUTE_VISIT_ID,visit_id);
        it.putExtra(RegisterFormFragment.ROUTE_FORM_DATA,form);
        startActivityForResult(it, RegisterFormActivity.CODE_RESULT);
    }
}