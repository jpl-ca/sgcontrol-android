package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.FormValuesE;
import com.jmt.sg.sgcontrol.presentation.adapter.FormToRegisterAdapter;
import com.jmt.sg.sgcontrol.presentation.presenter.RegisterFormPresenter;
import com.jmt.sg.sgcontrol.presentation.view.RegisterFormView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class RegisterFormFragment extends BaseFragment implements RegisterFormView {
    @BindString(R.string.s_registrar_formulario) protected String titlebar;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.rv_items) protected RecyclerView rv_items;

    LayoutInflater inflater;
    private Context context;
    private OnRegisterFormListener presenter;
    private FormToRegisterAdapter oAdapter;
    public RegisterFormPresenter registerFormPresenter;

    public static final String ROUTE_FORM_DATA = "RouteFormData";
    public static final String ROUTE_VISIT_ID = "RouteVisitId";

    public static RegisterFormFragment instance(){
        return new RegisterFormFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_form, container, false);
        ButterKnife.bind(this, rootView);
        setToolbar(toolbar, titlebar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        this.inflater = inflater;
        this.context = getContext();

        rv_items.setHasFixedSize(true);
        rv_items.setLayoutManager(new LinearLayoutManager(context));

        registerFormPresenter = new RegisterFormPresenter();
        registerFormPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnRegisterFormListener) {
            presenter = (OnRegisterFormListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item_search, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:presenter.onBackPressed();break;
            case R.id.action_send_form:sendFormToRegister();break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendFormToRegister() {
        registerFormPresenter.sendFormToRegister();
    }

    @Override
    public void showFormsList(final ArrayList<FormValuesE> items) {
        oAdapter = new FormToRegisterAdapter(getContext(), items, new FormToRegisterAdapter.ItemsListener() {
            @Override
            public void onFormValueChanged(int pos, String value) {
//                System.out.println("Actualizando "+items.get(pos).getLabel()+ " :"+value);
                registerFormPresenter.setFormInputData(pos,value);
            }
        });
        rv_items.setAdapter(oAdapter);
    }

    @Override
    public void showMessage(String message) {
        showMessage(ll_root,message);
    }

    @Override
    public void registerFromSent(ArrayList<FormValuesE> form_fields) {
        presenter.refreshRegisterForm(form_fields);
    }

    public interface OnRegisterFormListener {
        void onBackPressed();
        void refreshRegisterForm(ArrayList<FormValuesE> form_fields);
    }
}