package com.jmt.sg.sgcontrol.domain.model;

import java.io.Serializable;

/**
 * Created by jmtech on 5/16/16.
 */
public class LocationHistoryE implements Serializable {
    private double lat;
    private double lng;
    private double distance;

    public LocationHistoryE(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
    public LocationHistoryE(double lat, double lng, double distance) {
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "</"+lat + "," + lng + " | "+String.format("%.5f",distance) + "/>   ";
    }
}