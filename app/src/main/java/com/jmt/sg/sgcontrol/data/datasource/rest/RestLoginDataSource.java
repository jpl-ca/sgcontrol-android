package com.jmt.sg.sgcontrol.data.datasource.rest;

import android.content.Context;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.data.datasource.LoginDataSource;
import com.jmt.sg.sgcontrol.data.datasource.preferences.JMStore;
import com.jmt.sg.sgcontrol.data.datasource.rest.api.S;
import com.jmt.sg.sgcontrol.data.datasource.rest.service.AccessAccountService;
import com.jmt.sg.sgcontrol.data.model.LoginTrackeableResponse;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.net.ConnectException;

import retrofit.HttpException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by jmtech on 5/13/16.
 */
public class RestLoginDataSource implements LoginDataSource {

    private CompositeSubscription compositeSubscription;
    private AccessAccountService accessAccountService;
    private final Context context;
    private final JMStore jmStore;

    public RestLoginDataSource(Context context) {
        this.context= context;
        accessAccountService = new AccessAccountService(context);
        compositeSubscription = new CompositeSubscription();
        jmStore = new JMStore(context);
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    @Override
    public void loginAgent(String identification_code, String password, final RepositoryCallback repositoryCallback) {
        Subscription subscription = accessAccountService.getApi().userLoginAgent(identification_code, password).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<LoginTrackeableResponse>() {
                            @Override
                            public void call(LoginTrackeableResponse trackeable) {
                                saveToken(trackeable.getToken());
                                repositoryCallback.onSuccess(trackeable);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                ConnectException connEx;
                                HttpException httpEx;
                                throwable.printStackTrace();
                                if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(context.getString(R.string.s_error_de_conexion)+" Posiblemente wifi esta desconectado o ip no existe");
                                    repositoryCallback.onError(context.getString(R.string.s_error_de_conexion));
                                } else if (throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->" + str);
                                        repositoryCallback.onError(str);
                                    } catch (IOException e) {
                                        repositoryCallback.onError(context.getString(R.string.s_error_de_conexion));
                                    }
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }

    private void saveToken(String token) {
        jmStore.saveString(S.VAR.TOKEN, token);
    }
}