package com.jmt.sg.sgcontrol.data.datasource;

import android.content.Context;

import com.jmt.sg.sgcontrol.data.datasource.db.DbRouteGeolocationDataSource;
import com.jmt.sg.sgcontrol.data.datasource.rest.RestRouteGeolocationDataSource;
import com.jmt.sg.sgcontrol.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class RouteGeolocationDataSourceFactory {
    private final Context context;

    public RouteGeolocationDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public RouteGeolocationDataSource create(DataSourceFactory dataSource) {

        RouteGeolocationDataSource geolocationDataSource = null;
        switch (dataSource) {
            case CLOUD:
                geolocationDataSource = new RestRouteGeolocationDataSource(context);
                break;
            case DB:
                geolocationDataSource = new DbRouteGeolocationDataSource(context);
                break;
        }
        return geolocationDataSource;
    }
}