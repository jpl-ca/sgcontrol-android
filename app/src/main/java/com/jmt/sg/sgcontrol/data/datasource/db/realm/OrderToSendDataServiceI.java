package com.jmt.sg.sgcontrol.data.datasource.db.realm;

import com.jmt.sg.sgcontrol.data.model.FormToSendEntity;

import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface OrderToSendDataServiceI {
    Observable<FormToSendEntity> getOrderToSend(long visit_id);
    Observable<Boolean> registerOrderToSend(long visit_id, FormToSendEntity user);
    Observable<Boolean> removeOrderToSend(long visit_id);
}