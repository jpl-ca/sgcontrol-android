package com.jmt.sg.sgcontrol.data.datasource;

import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface LoginDataSource {
    void loginAgent(String identification_code, String password, RepositoryCallback repositoryCallback);
}