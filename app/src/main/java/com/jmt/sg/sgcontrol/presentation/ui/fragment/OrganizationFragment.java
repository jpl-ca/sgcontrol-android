package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.presenter.OrganizationPresenter;
import com.jmt.sg.sgcontrol.presentation.view.OrganizationView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class OrganizationFragment extends BaseFragment implements OrganizationView {
    @Bind(R.id.txt_organization) AppCompatEditText txt_organization;
    @Bind(R.id.btn_verificar) Button btn_verificar;
    @Bind(R.id.ll_root) LinearLayout ll_root;

    private OnOrganizationListener presenter;
    private OrganizationPresenter organizationPresenter;

    public static OrganizationFragment instance(){
        return new OrganizationFragment();
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnOrganizationListener) {
            presenter = (OnOrganizationListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_organization, container, false);
        injectView(rootView);

        organizationPresenter = new OrganizationPresenter();
        organizationPresenter.addView(this);

        return rootView;
    }


    @OnClick(R.id.btn_verificar)
    public void verify(){
        String organization = txt_organization.getText().toString();
        organizationPresenter.validateOrganization(organization);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        organizationPresenter.removeView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    @Override
    public void showLoading() {
        btn_verificar.setEnabled(false);
        txt_organization.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        btn_verificar.setEnabled(true);
        txt_organization.setEnabled(true);
    }

    @Override
    public void successValidate() {
        presenter.nextActivity();
    }

    @Override
    public void showErrorMessage(String message) {
        showMessage(ll_root,message);
    }

    public interface OnOrganizationListener{
        void nextActivity();
    }
}