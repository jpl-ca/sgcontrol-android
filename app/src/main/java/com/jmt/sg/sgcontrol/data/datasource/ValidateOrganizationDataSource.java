package com.jmt.sg.sgcontrol.data.datasource;

import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface ValidateOrganizationDataSource {
    void validateOrganization(String organization_name, RepositoryCallback repositoryCallback);
}