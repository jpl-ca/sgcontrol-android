package com.jmt.sg.sgcontrol.tracking.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jmt.sg.sgcontrol.presentation.presenter.RouteGeolocationPresenter;
import com.jmt.sg.sgcontrol.presentation.view.RouteGeolocationView;
import com.jmt.sg.sgcontrol.tracking.GpsConstant;

/**
 * Created by jmtech on 6/1/16.
 */
public class GeoLocationReceiver extends BroadcastReceiver implements RouteGeolocationView{
    private final String TAG = "GeoLocationReceiver";
    private Context ctx;
    private RouteGeolocationPresenter routeGeolocationPresenter;

    public GeoLocationReceiver(){
        routeGeolocationPresenter = new RouteGeolocationPresenter();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(GpsConstant.gpsLocation==null){
            System.out.println("</*******************|*.*|******************/>");
            return;
        }
        ctx = context;
        routeGeolocationPresenter.addView(this);
        System.out.println("</*************************************");
        routeGeolocationPresenter.registerRouteGeolocation(GpsConstant.gpsLocation.getLatitude(),GpsConstant.gpsLocation.getLongitude());
        System.out.println("*************************************/>");
    }

    @Override
    public Context getContext() {
        return ctx;
    }
}