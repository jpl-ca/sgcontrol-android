package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.FormValuesE;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.RegisterFormFragment;

import java.util.ArrayList;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class RegisterFormActivity extends AppCompatActivity implements RegisterFormFragment.OnRegisterFormListener {

    public static final int CODE_RESULT = 101;

    private final RegisterFormFragment registerFormFragment = RegisterFormFragment.instance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_form);
        showFragment();
    }

    private void showFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, registerFormFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void refreshRegisterForm(ArrayList<FormValuesE> form_fields) {
        Intent it = new Intent();
        it.putExtra(RegisterFormFragment.ROUTE_FORM_DATA, form_fields);
        setResult(RESULT_OK,it);
    }
}