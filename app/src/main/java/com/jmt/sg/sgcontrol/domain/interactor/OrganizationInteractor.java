package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.repository.OrganizationRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class OrganizationInteractor {
    private final OrganizationRepository orgRepository;

    public OrganizationInteractor(OrganizationRepository placeRepository) {
        this.orgRepository = placeRepository;
    }

    public void validateOrganization(String organization_name, final Callback placeCallback) {
        orgRepository.validateOrganization(organization_name, placeCallback);
    }

    public interface Callback {
        void onValidateSuccess();
        void onValidateError(String message);
    }
}