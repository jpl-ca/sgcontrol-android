package com.jmt.sg.sgcontrol.presentation.ui.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.ui.dialog.callback.CallbackInputNumberDialog;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by JMTech-Android on 19/10/2015.
 */
public class InputNumberDialog {
    private AlertDialog.Builder builder;
    private AlertDialog dlg;
    private Context ctx;
    @Bind(R.id.txt_ammount) NumberPicker txt_ammount;

    public InputNumberDialog(Context ctx){
        this.ctx = ctx;
        builder = new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(ctx.getString(R.string.seleccione_cantidad));
    }

    public void hideDialog() {
        dlg.dismiss();
    }

    public void showInputNumberDialog(int value,final CallbackInputNumberDialog _cb){
        View view = LayoutInflater.from(ctx).inflate(R.layout.dialog_input_number, null, false);
        ButterKnife.bind(this, view);
        txt_ammount.setMaxValue(250);
        txt_ammount.setMinValue(1);
        txt_ammount.setValue(value);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE: hideDialog();_cb.confirm(txt_ammount.getValue()); break;
                    case DialogInterface.BUTTON_NEGATIVE: _cb.cancel(); break;
                }
            }
        };
        builder.setCancelable(true);
        builder.setView(view);
        builder.setPositiveButton(ctx.getString(R.string.aceptar), dialogClickListener);
        builder.setNegativeButton(ctx.getString(R.string.cancelar), dialogClickListener);
        dlg = builder.create();
        dlg.show();
    }

}