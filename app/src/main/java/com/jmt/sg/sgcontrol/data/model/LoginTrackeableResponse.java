package com.jmt.sg.sgcontrol.data.model;

import com.jmt.sg.sgcontrol.data.mapper.BaseResponse;

/**
 * Created by jmtech on 5/16/16.
 */
public class LoginTrackeableResponse extends BaseResponse {
    private int id;

    /** Agent*/

    private String authentication_code;

    private String first_name;

    private String last_name;



    /** Vehicle*/

    private String plate;

    private String brand;

    private String model;

    private String color;

    private String manufacture_year;

    private String gas_consumption_rate;




    /** Common*/

    private String organization_id;

    private String type;

    private String token;

    private int location_frequency;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthentication_code() {
        return authentication_code;
    }

    public void setAuthentication_code(String authentication_code) {
        this.authentication_code = authentication_code;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getManufacture_year() {
        return manufacture_year;
    }

    public void setManufacture_year(String manufacture_year) {
        this.manufacture_year = manufacture_year;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getLocation_frequency() {
        return location_frequency;
    }

    public void setLocation_frequency(int location_frequency) {
        this.location_frequency = location_frequency;
    }

    public String getGas_consumption_rate() {
        return gas_consumption_rate;
    }

    public void setGas_consumption_rate(String gas_consumption_rate) {
        this.gas_consumption_rate = gas_consumption_rate;
    }
}