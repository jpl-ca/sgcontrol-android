package com.jmt.sg.sgcontrol.domain.interactor;

import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;
import com.jmt.sg.sgcontrol.domain.model.FormToSendE;
import com.jmt.sg.sgcontrol.domain.repository.OrderToSendRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class OrderToSendInteractor {
    private final OrderToSendRepository orderToSendRepository;

    public OrderToSendInteractor(OrderToSendRepository orderToSendRepository) {
        this.orderToSendRepository = orderToSendRepository;
    }

    public void getOrderToSend(int visit_id, final Callback callback) {
        orderToSendRepository.getOrderToSend(visit_id, callback);
    }

    public void saveOrderToSend(int visit_id, FormToSendE orderToSend, final Callback callback) {
        orderToSendRepository.saveOrderToSend(visit_id, orderToSend, callback);
    }

    public void removeOrderToSend(int visit_id, final Callback callback) {
        orderToSendRepository.removeOrderToSend(visit_id, callback);
    }

    public interface Callback {
        void onOrderToSendSuccess(AgentTrackeable agentTrackeable);
        void onOrderToSendError(String message);
    }
}