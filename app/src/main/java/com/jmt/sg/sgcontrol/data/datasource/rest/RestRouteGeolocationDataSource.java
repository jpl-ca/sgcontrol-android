package com.jmt.sg.sgcontrol.data.datasource.rest;

import android.content.Context;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.data.datasource.RouteGeolocationDataSource;
import com.jmt.sg.sgcontrol.data.datasource.preferences.JMStore;
import com.jmt.sg.sgcontrol.data.datasource.rest.service.GeoLocationService;
import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;

import retrofit.HttpException;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by jmtech on 5/13/16.
 */
public class RestRouteGeolocationDataSource implements RouteGeolocationDataSource {
    private CompositeSubscription compositeSubscription;
    private GeoLocationService geoLocationService;
    private final Context context;
    private final JMStore jmStore;

    public RestRouteGeolocationDataSource(Context context) {
        this.context= context;
        geoLocationService = new GeoLocationService(context);
        compositeSubscription = new CompositeSubscription();
        jmStore = new JMStore(context);
    }

    public void finish(){
        compositeSubscription.unsubscribe();
    }

    @Override
    public void storeGeolocation(double lat, double lng, final RepositoryCallback repositoryCallback) {
        Subscription subscription = geoLocationService.getApi().storeGeolocation(lat,lng).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(
                        new Action1<JSONObject>() {
                            @Override
                            public void call(JSONObject trackeable) {
                                repositoryCallback.onSuccess(trackeable);
                            }
                        },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                ConnectException connEx;
                                HttpException httpEx;
                                throwable.printStackTrace();
                                if (throwable instanceof ConnectException) {
                                    connEx = ((ConnectException) throwable);
                                    System.out.println(context.getString(R.string.s_error_de_conexion)+" Posiblemente wifi esta desconectado o ip no existe");
                                    repositoryCallback.onError(context.getString(R.string.s_error_de_conexion));
                                } else if (throwable instanceof HttpException) {
                                    httpEx = ((HttpException) throwable);
                                    System.out.println(httpEx.code());
                                    ResponseBody responseBody = httpEx.response().errorBody();
                                    try {
                                        String str = responseBody.string();
                                        System.out.println("--->" + str);
                                        repositoryCallback.onError(str);
                                    } catch (IOException e) {
                                        repositoryCallback.onError(context.getString(R.string.s_error_de_conexion));
                                    }
                                }
                            }
                        }
                );
        if (compositeSubscription != null) {
            compositeSubscription.add(subscription);
        }
    }
}