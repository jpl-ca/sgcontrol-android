package com.jmt.sg.sgcontrol.tracking.data_source;

import android.location.Location;

import rx.Observable;

/**
 * Created by jmtech on 5/24/16.
 */
public interface GpsDataServiceI {
    void startGpsService();
    void stopGpsService();
    Observable<Location> getLocationUpdates();
}