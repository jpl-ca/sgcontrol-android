package com.jmt.sg.sgcontrol.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.presentation.ui.fragment.SplashFragment;

public class SplashActivity extends BaseActivity implements SplashFragment.OnSplashListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void toLoginActivity() {
        startActivity(new Intent(this,OrganizationActivity.class));
        finish();
    }

    @Override
    public void toHomeActivity() {
        startActivity(new Intent(this,MapHomeActivity.class));
        finish();
    }
}