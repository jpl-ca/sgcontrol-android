package com.jmt.sg.sgcontrol.presentation.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.jmt.sg.sgcontrol.R;
import com.jmt.sg.sgcontrol.domain.model.FormE;
import com.jmt.sg.sgcontrol.domain.model.FormValuesE;
import com.jmt.sg.sgcontrol.domain.model.RouteVisitE;
import com.jmt.sg.sgcontrol.presentation.presenter.ConfirmDialogPresenter;
import com.jmt.sg.sgcontrol.presentation.presenter.VisitOrderPresenter;
import com.jmt.sg.sgcontrol.presentation.utils.Constants;
import com.jmt.sg.sgcontrol.presentation.view.ConfirmDialogView;
import com.jmt.sg.sgcontrol.presentation.view.VisitOrderView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class VisitOrderFragment extends BaseFragment implements VisitOrderView{
    @BindString(R.string.t_detalle_visita) protected String titlebar;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.toolbar) protected Toolbar toolbar;

    @Bind(R.id.txt_cliente) protected TextView txt_cliente;
    @Bind(R.id.txt_telefono) protected TextView txt_telefono;
    @Bind(R.id.txt_direccion) protected TextView txt_direccion;
    @Bind(R.id.txt_referencia) protected TextView txt_referencia;
    @Bind(R.id.tv_state_visit_point) protected TextView tv_state_visit_point;

    @Bind(R.id.btn_reprogramar) protected Button btn_reprogramar;
    @Bind(R.id.btn_load_form) protected Button btn_load_form;

    LayoutInflater inflater;
    private Context context;
    private OnVisitOrderListener presenter;

    private ConfirmDialogPresenter confirmDialogPresenter;

    public static final String ROUTE_VISIT_DATA = "RouteVisitData";
    public VisitOrderPresenter visitOrderPresenter;

    private int VISIT_ORDER_STATE = -1;

    public static VisitOrderFragment instance(){
        return new VisitOrderFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_visit_order, container, false);
        ButterKnife.bind(this, rootView);
        setToolbar(toolbar, titlebar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        this.inflater = inflater;
        this.context = getContext();

        visitOrderPresenter = new VisitOrderPresenter();
        visitOrderPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnVisitOrderListener) {
            presenter = (OnVisitOrderListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @OnClick(R.id.btn_goto_direcction)
    protected void goToDirection() {
        visitOrderPresenter.getMyLocation(new VisitOrderPresenter.onGetMyLocation() {
            @Override
            public void setMyLocation(LatLng latlng) {
                RouteVisitE visit = visitOrderPresenter.getRouteVisit();

                double MeLat = latlng.latitude;
                double MeLng = latlng.longitude;

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+MeLat+","+MeLng+"&daddr="+visit.getLat()+","+visit.getLng()));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:presenter.onBackPressed();break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_reprogramar)
    protected void reschedule() {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {}
            @Override
            public void clickConfirm(String input) {
                visitOrderPresenter.rescheduleVisitPoint(input);
            }
            @Override
            public void clickCancel() {}
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showInputDialog(context.getString(R.string.s_se_guardara_como_reprogramado));
    }

    @OnClick(R.id.btn_load_form)
    public void loadItemsActivity() {
        if(VISIT_ORDER_STATE == Constants.VISIT_STATE.Scheduled)
            presenter.loadFormActivity(visitOrderPresenter.getRouteVisit().getId(),visitOrderPresenter.getFormToResponse());
    }

    public void settingData(RouteVisitE routeVisitE) {
        txt_cliente.setText(routeVisitE.getName());
        txt_telefono.setText(routeVisitE.getPhone());
        txt_direccion.setText(routeVisitE.getAddress());
        txt_referencia.setText(routeVisitE.getReference());
    }

    @Override
    public void onRequestError(String message) {
        showMessage(ll_root,message);
    }

    @Override
    public void setVisitPointState(int state) {
        VISIT_ORDER_STATE = state;
        if(VISIT_ORDER_STATE == Constants.VISIT_STATE.ReScheduling){
            btn_reprogramar.setEnabled(false);
            btn_load_form.setEnabled(false);
            tv_state_visit_point.setText(getString(R.string.state_reprogramado));
        }else if(VISIT_ORDER_STATE == Constants.VISIT_STATE.Scheduled){
            tv_state_visit_point.setText(getString(R.string.state_programado));
        }
    }

    @Override
    public void showMessage(String message) {
        showMessage(ll_root,message);
    }

    @Override
    public void visitPointRescheduled() {
        /** Actualiza el TimelineActivity al cambiar el estado de la visita */
        presenter.sendResultSuccess(visitOrderPresenter.getRouteVisit());
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void refreshRouteForm(ArrayList<FormValuesE> form_refresh) {
        /** Actualiza los datos de la visita */
        visitOrderPresenter.refreshRouteForm(form_refresh);

        /** Actualiza el TimelineActivity si es necesario */
        presenter.sendResultSuccess(visitOrderPresenter.getRouteVisit());
    }

    public interface OnVisitOrderListener{
        void sendResultSuccess(RouteVisitE routeVisit);
        void loadFormActivity(long visit_id, FormE form);
        void onBackPressed();
    }
}