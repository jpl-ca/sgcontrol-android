package com.jmt.sg.sgcontrol.data.datasource;

import com.jmt.sg.sgcontrol.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface RescheduleVisitDataSource {
    void rescheduleVisit(long visit_point_id, String comment, final RepositoryCallback repositoryCallback);
}