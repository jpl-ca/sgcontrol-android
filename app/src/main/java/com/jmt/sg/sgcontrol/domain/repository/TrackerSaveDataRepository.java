package com.jmt.sg.sgcontrol.domain.repository;

import com.jmt.sg.sgcontrol.domain.interactor.TrackerSaveInteractor;
import com.jmt.sg.sgcontrol.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackerSaveDataRepository {
    void saveTrackerData(AgentTrackeable aTrackeable, TrackerSaveInteractor.Callback aCallback);
}